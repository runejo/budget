import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/database/datamodels/sync_db.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class SearchSuggestions {
  static String tableSearchSuggestionsProducts = 'search_suggestions_products';
  static String tableSearchSuggestionsStores = 'search_suggestions_stores';

  static Future<void> createTables(Database db) async {
    await db.execute('''
          CREATE TABLE $tableSearchSuggestionsProducts (
            product TEXT,
            productCategory TEXT,
            productCode TEXT,
            lastAdded INT,
            count INT)
          ''');

    await db.execute('''
          CREATE TABLE $tableSearchSuggestionsStores (
            storeName TEXT,
            storeType TEXT,
            lastAdded INT,
            count INT)
          ''');
  }

  //Product related code
  static Future<List<Map<String, dynamic>>> findProductCount(
      String productCode) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> result = await db.rawQuery(
        "Select count FROM $tableSearchSuggestionsProducts WHERE productCode='$productCode'");
    return result;
  }

  static addProduct(String product, String productCategory, String productCode,
      int newCount) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> oldCount = await findProductCount(productCode);
    //Product has not been seen before
    if (oldCount.length == 0) {
      Map<String, dynamic> data = Map<String, dynamic>();
      data['product'] = product;
      data['productCategory'] = productCategory;
      data['productCode'] = productCode;
      data['lastAdded'] = DateTime.now().millisecondsSinceEpoch;
      data['count'] = newCount;
      await db.insert(tableSearchSuggestionsProducts, data);
      await SyncDatabase.createSync(PRODUCT_TYPE, productCode, CREATED);
    } else {
      //Product has been seen before
      await db.rawUpdate(
          "UPDATE $tableSearchSuggestionsProducts SET count=${oldCount[0]['count'] + newCount} WHERE productCode='$productCode'");
      await db.rawUpdate(
          "UPDATE $tableSearchSuggestionsProducts SET lastAdded=${DateTime.now().millisecondsSinceEpoch} WHERE productCode='$productCode'");
      await SyncDatabase.updateSync(PRODUCT_TYPE, productCode, UPDATED);
    }
  }

  static removeProduct(String productCode, int newCount) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> oldCount = await findProductCount(productCode);
    await db.rawUpdate(
        "UPDATE $tableSearchSuggestionsProducts SET count=${oldCount[0]['count'] - newCount} WHERE productCode='$productCode'");
    await SyncDatabase.updateSync(PRODUCT_TYPE, productCode, UPDATED);
  }

  static Future<List<Map<String, dynamic>>> getMostRecentProducts() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> result = await db.rawQuery(
        "Select * FROM $tableSearchSuggestionsProducts ORDER BY lastAdded DESC");
    return result;
  }

  static Future<List<Map<String, dynamic>>> getMostFrequentProducts() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> result = await db.rawQuery(
        "Select * FROM $tableSearchSuggestionsProducts ORDER BY count DESC");
    return result;
  }

  static Future<List<Map<String, dynamic>>> querySearchProduct(
      String productCode) async {
    Database db = await DatabaseHelper.instance.database;
    return await db.query(
      tableSearchSuggestionsProducts,
      where: "productCode = ?",
      whereArgs: [productCode],
    );
  }

  //Store related code
  static Future<List<Map<String, dynamic>>> findStoreCount(
      String storeName) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> result = await db.rawQuery(
        "Select count FROM $tableSearchSuggestionsStores WHERE storeName='$storeName'");
    return result;
  }

  static addStore(String storeName, String storeType) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> oldCount = await findStoreCount(storeName);
    //Store has not been seen before
    if (oldCount.length == 0) {
      Map<String, dynamic> data = Map<String, dynamic>();
      data['storeName'] = storeName;
      data['storeType'] = storeType;
      data['lastAdded'] = DateTime.now().millisecondsSinceEpoch;
      data['count'] = 1;
      await db.insert(tableSearchSuggestionsStores, data);
      await SyncDatabase.createSync(STORE_TYPE, storeName, CREATED);
    } else {
      //Product has been seen before
      await db.rawUpdate(
          "UPDATE $tableSearchSuggestionsStores SET count=${oldCount[0]['count'] + 1} WHERE storeName='$storeName'");
      await db.rawUpdate(
          "UPDATE $tableSearchSuggestionsStores SET lastAdded=${DateTime.now().millisecondsSinceEpoch} WHERE storeName='$storeName'");
      await SyncDatabase.updateSync(STORE_TYPE, storeName, UPDATED);
    }
  }

  static Future<List<Map<String, dynamic>>> getMostRecentStores() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> result = await db.rawQuery(
        "Select * FROM $tableSearchSuggestionsStores ORDER BY lastAdded DESC");
    return result;
  }

  static Future<List<Map<String, dynamic>>> getMostFrequentStores() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> result = await db.rawQuery(
        "Select * FROM $tableSearchSuggestionsStores ORDER BY count DESC");
    return result;
  }

  static Future<List<Map<String, dynamic>>> querySearchStore(
      String storeName) async {
    Database db = await DatabaseHelper.instance.database;
    return await db.query(
      tableSearchSuggestionsStores,
      where: "storeName = ?",
      whereArgs: [storeName],
    );
  }
}
