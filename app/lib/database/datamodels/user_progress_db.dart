import 'dart:async';
import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserProgressDatabase {
  static Map<String, List<DateTime>> dayValues;

  static Future<void> initializeData() async {
    Database db = await DatabaseHelper.instance.database;
    List<String> days = _createDaysOfExperiment();
    days.forEach((day) {
      db.insert('user_progress', {'daysOfExperiment': day});
    });
    setInitializedToTrue();
  }

  static void setInitializedToTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('initialized', true);
  }

  static Future<bool> isInitialized() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('initialized' ?? false as String) != null;
  }

  static List<String> _createDaysOfExperiment() {
    DateTime now = DateTime.now();
    List<String> days = [];
    for (int i = 0; i <= 7; i++) {
      DateTime _nextDate = now.add(new Duration(days: i));
      days.add(DateFormat('yyyyMMdd').format(_nextDate));
    }
    return days;
  }

  static Future<Map<String, List<DateTime>>> getCalendarFormatting() async {
    List<DateTime> daysOfExperiment = await getDaysOfExperiment();
    List<DateTime> daysCompleted = await getDaysCompleted();
    List<DateTime> daysMissing = await getDaysMissing();
    dayValues = {
      'daysOfExperiment': daysOfExperiment,
      'daysCompleted': daysCompleted,
      'daysMissing': daysMissing,
    };
    return dayValues;
  }

  static Future<List<DateTime>> getDaysOfExperiment() async {
    Database db = await DatabaseHelper.instance.database;

    List<Map<String, dynamic>> result =
        await db.rawQuery('SELECT * FROM user_progress');

    List<DateTime> daysOfExperiment = [];
    result.forEach((Map<String, dynamic> day) {
      if (day['daysOfExperiment'] != null) {
        daysOfExperiment
            .add(DateTime.parse(day['daysOfExperiment'].toString()));
      }
    });

    return daysOfExperiment;
  }

  static Future<List<DateTime>> getDaysCompleted() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> result =
        await db.rawQuery('SELECT * FROM user_progress');

    List<DateTime> daysCompleted = [];
    result.forEach((Map<String, dynamic> day) {
      if (day['daysCompleted'] != null) {
        daysCompleted.add(DateTime.parse(day['daysCompleted'].toString()));
      }
    });
    return daysCompleted;
  }

  static Future<List<DateTime>> getDaysMissing() async {
    DateTime today = DateTime.now();
    List<DateTime> daysOfExperiment = await getDaysOfExperiment();
    List<DateTime> daysCompleted = await getDaysCompleted();
    List<DateTime> daysMissing = [];
    daysOfExperiment.forEach((date) {
      if (date.difference(today).inDays < 0) {
        if (daysCompleted.contains(date) == false) {
          daysMissing.add(date);
        }
      }
    });
    return daysMissing;
  }

  static Future<Map<String, String>> getProgressStats() async {
    List<DateTime> daysCompleted = await getDaysCompleted();
    String completed = daysCompleted.length.toString();

    List<DateTime> daysMissing = await getDaysMissing();
    String missing = daysMissing.length.toString();

    List<DateTime> daysOfExperiment = await getDaysOfExperiment();
    DateTime lastDate = daysOfExperiment[daysOfExperiment.length - 1];
    int difference = await isDayCompleted(DateTime.now().year.toString(),
            DateTime.now().month.toString(), DateTime.now().day.toString())
        ? 0
        : 1;
    String remaining =
        (lastDate.difference(DateTime.now()).inDays + difference).toString();

    return {'completed': completed, 'missing': missing, 'remaining': remaining};
  }

  static Future<Map<String, String>> getStartAndEndDateExperiment() async {
    List<DateTime> datesOfExperiment = await getDaysOfExperiment();
    String startDate = DateFormat('d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
        .format(datesOfExperiment[0]);
    String endDate = DateFormat('d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
        .format(datesOfExperiment[datesOfExperiment.length - 1]);

    return {'startDate': startDate, 'endDate': endDate};
  }

  static Future<void> markDayComplete(String year, String month, String day) async {
    Database db = await DatabaseHelper.instance.database;
    bool isNew = await isDayCompleted(year, month, day) == false;
    if (isNew) {
      db.insert(
          'user_progress', {'daysCompleted': getDateString(year, month, day)});
    }
  }

  Future<void> unmarkDayComplete(String year, String month, String day) async {
    Database db = await DatabaseHelper.instance.database;
    db.delete('user_progress',
        where: 'daysCompleted = ?',
        whereArgs: [getDateString(year, month, day)]);
  }

  static Future<bool> isDayCompleted(String year, String month, String day) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> dayComplete = await db.rawQuery(
        'SELECT * FROM user_progress WHERE daysCompleted = ${getDateString(year, month, day)}');
    return (dayComplete.length > 0);
  }

  static String getDateString(String year, String month, String day) {
    month = month.length == 1 ? '0$month' : month;
    day = day.length == 1 ? '0$day' : day;
    return ('$year$month$day');
  }

  static Future<String> canCompleteDay(
      String completeDay, String year, String month, String day) async {
    bool dayIsCompleted = await isDayCompleted(year, month, day);

    if (dayIsCompleted) {
      return 'alreadyComplete';
    }

    List<DateTime> daysOfExperiment = await getDaysOfExperiment();
    bool isInExperiment = false;
    daysOfExperiment.forEach(
      (date) {
        if (int.parse(DateFormat('yyyyMMdd').format(date)) ==
            int.parse(completeDay)) {
          isInExperiment = true;
        }
      },
    );

    int today = int.parse(DateFormat('yyyyMMdd').format(DateTime.now()));
    bool isInPast = int.parse(completeDay) <= today;

    if (isInExperiment && isInPast) {
      return 'canComplete';
    }
    return 'cannotComplete';
  }
}
