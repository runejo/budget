class SyncId {
  String phoneGuid = "";
  String phoneName = "";
  String userGuid = "";
}

class SyncUser {
  SyncUser(this.id, this.guid, this.name, this.password, this.update);

  SyncUser.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        guid = json['guid'],
        name = json['name'],
        password = json['password'],
        update = json['update'];

  String guid;
  int id;
  String name;
  String password;
  int update;
}

class SyncPhone {
  SyncPhone(this.id, this.guid, this.userId, this.name, this.update);

  SyncPhone.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        guid = json['guid'],
        userId = json['userId'],
        name = json['name'],
        update = json['update'];

  String guid;
  int id;
  String name;
  int update;
  int userId;
}

class Sync {
  Sync(
      this.dataType, this.dataIdentifier, this.update, this.timestamp, this.action);

  Sync.fromJson(Map<String, dynamic> json)
      : dataType = json['dataType'],
        dataIdentifier = json['dataIdentifier'],
        update = json['update'],
        timestamp = json['timestamp'],
        action = json['action'];

  int action;
  String dataIdentifier;
  int dataType;
  int timestamp;
  int update;

  Map<String, dynamic> toJson() => {
        'dataType': dataType,
        'dataIdentifier': dataIdentifier,
        'update': update,
        'timestamp': timestamp,
        'action': action
      };
}

class SyncTransaction {
  SyncTransaction(
      this.transactionID,
      this.store,
      this.storeType,
      this.date,
      this.discountAmount,
      this.discountPercentage,
      this.expenseAbroad,
      this.expenseOnline,
      this.totalPrice,
      this.discountText,
      this.receiptLocation,
      this.receiptProductType);

  SyncTransaction.fromJson(Map<String, dynamic> json)
      : transactionID = json['transactionID'],
        store = json['store'],
        storeType = json['storeType'],
        date = json['date'],
        discountAmount = json['discountAmount'],
        discountPercentage = json['discountPercentage'],
        expenseAbroad = json['expenseAbroad'],
        expenseOnline = json['expenseOnline'],
        totalPrice = json['totalPrice'] + 0.0,
        discountText = json['discountText'],
        receiptLocation = json['receiptLocation'],
        receiptProductType = json['receiptProductType'];

  SyncTransaction.fromQuerys(Map<String, dynamic> trans) {
    transactionID = trans['transactionID'];
    store = trans['store'];
    storeType = trans['storeType'];
    date = trans['date'];
    discountAmount = trans['discountAmount'];
    discountPercentage = trans['discountPercentage'];
    expenseAbroad = trans['expenseAbroad'];
    expenseOnline = trans['expenseOnline'];
    totalPrice = trans['totalPrice'] + 0.0;
    discountText = trans['discountText'];
    receiptLocation = trans['receiptLocation'];
    receiptProductType = trans['receiptProductType'];
  }

  int date;
  String discountAmount;
  String discountPercentage;
  String discountText;
  String expenseAbroad;
  String expenseOnline;
  String receiptLocation;
  String receiptProductType;
  String store;
  String storeType;
  double totalPrice;
  String transactionID;

  Map<String, dynamic> toJson() => {
        'transactionID': transactionID,
        'store': store,
        'storeType': storeType,
        'date': date,
        'discountAmount': discountAmount,
        'discountPercentage': discountPercentage,
        'expenseAbroad': expenseAbroad,
        'expenseOnline': expenseOnline,
        'totalPrice': totalPrice,
        'discountText': discountText,
        'receiptLocation': receiptLocation,
        'receiptProductType': receiptProductType
      };
}

class SyncProduct {
  SyncProduct(this.transactionID, this.product, this.productCategory,
      this.price, this.productCode, this.productDate, this.productGroupID);

  SyncProduct.fromJson(Map<String, dynamic> json)
      : transactionID = json['transactionID'],
        product = json['product'],
        productCategory = json['productCategory'],
        price = json['price'] + 0.0,
        productCode = json['productCode'],
        productDate = json['productDate'],
        productGroupID = json['productGroupID'];

  SyncProduct.fromQuery(Map<String, dynamic> prod)
      : transactionID = prod['transactionID'],
        product = prod['product'],
        productCategory = prod['productCategory'],
        price = prod['price'] + 0.0,
        productCode = prod['productCode'],
        productDate = prod['productDate'],
        productGroupID = prod['productGroupID'];

  double price;
  String product;
  String productCategory;
  String productCode;
  String productDate;
  String productGroupID;
  String transactionID;

  Map<String, dynamic> toJson() => {
        'transactionID': transactionID,
        'product': product,
        'productCategory': productCategory,
        'price': price,
        'productCode': productCode,
        'productDate': productDate,
        'productGroupID': productGroupID
      };
}

class SyncImage {
  SyncImage(this.transactionID, this.base64image);

  SyncImage.fromJson(Map<String, dynamic> json)
      : transactionID = json['transactionID'],
        base64image = json['base64image'];

  SyncImage.fromQuery(Map<String, dynamic> prod)
      : transactionID = prod['transactionID'],
        base64image = prod['base64image'];

  String base64image;
  String transactionID;

  Map<String, dynamic> toJson() =>
      {'transactionID': transactionID, 'base64image': base64image};
}

class SyncReceiptData {
  SyncReceiptData(this.sync, this.transaction, this.products, this.image);

  SyncReceiptData.fromJson(Map<String, dynamic> json) {
    var list = json['products'] as List;
    List<SyncProduct> productList =
        list == null ? null : list.map((i) => SyncProduct.fromJson(i)).toList();

    sync = Sync.fromJson(json['synchronisation']);
    transaction = SyncTransaction.fromJson(json['transaction']);
    products = productList;
    image = SyncImage.fromJson(json['image']);
  }

  SyncImage image;
  List<SyncProduct> products;
  Sync sync;
  SyncTransaction transaction;

  Map<String, dynamic> toJson() => {
        'synchronisation': sync,
        'transaction': transaction,
        'products': products,
        'image': image
      };
}

class SyncSearchProduct {
  SyncSearchProduct(
      this.product,
      this.productCategory,
      this.productCode,
      this.lastAdded,
      this.count);

  SyncSearchProduct.fromJson(Map<String, dynamic> json)
      : product = json['product'],
        productCategory = json['productCategory'],
        productCode = json['productCode'],
        lastAdded = json['lastAdded'],
        count = json['count'];

  SyncSearchProduct.fromQuerys(Map<String, dynamic> trans) {
    product = trans['product'];
    productCategory = trans['productCategory'];
    productCode = trans['productCode'];
    lastAdded = trans['lastAdded'];
    count = trans['count'];
  }

  int count;
  int lastAdded;
  String product;
  String productCategory;
  String productCode;

  Map<String, dynamic> toJson() => {
        'product': product,
        'productCategory': productCategory,
        'productCode': productCode,
        'lastAdded': lastAdded,
        'count': count
      };
}

class SyncSearchStore {
  SyncSearchStore(
      this.storeName,
      this.storeType,
      this.lastAdded,
      this.count);

  SyncSearchStore.fromJson(Map<String, dynamic> json)
      : storeName = json['storeName'],
        storeType = json['storeType'],
        lastAdded = json['lastAdded'],
        count = json['count'];

  SyncSearchStore.fromQuerys(Map<String, dynamic> trans) {
    storeName = trans['storeName'];
    storeType = trans['storeType'];
    lastAdded = trans['lastAdded'];
    count = trans['count'];
  }

  int count;
  int lastAdded;
  String storeName;
  String storeType;

  Map<String, dynamic> toJson() => {
        'storeName': storeName,
        'storeType': storeType,
        'lastAdded': lastAdded,
        'count': count
      };
}

class SyncSearchProductData {
  SyncSearchProductData(this.sync, this.searchProduct);

  SyncSearchProductData.fromJson(Map<String, dynamic> json) {
    sync = Sync.fromJson(json['synchronisation']);
    searchProduct = SyncSearchProduct.fromJson(json['searchProduct']);
  }

  SyncSearchProduct searchProduct;
  Sync sync;

  Map<String, dynamic> toJson() => {
        'synchronisation': sync,
        'searchProduct': searchProduct
      };
}

class SyncSearchStoreData {
  SyncSearchStoreData(this.sync, this.searchStore);

  SyncSearchStoreData.fromJson(Map<String, dynamic> json) {
    sync = Sync.fromJson(json['synchronisation']);
    searchStore = SyncSearchStore.fromJson(json['searchStore']);
  }

  SyncSearchStore searchStore;
  Sync sync;

  Map<String, dynamic> toJson() => {
        'synchronisation': sync,
        'searchStore': searchStore
      };
}