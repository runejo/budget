import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'dart:io' show Platform;
import 'package:budget_onderzoek/translations/translations.dart';

class SettingsModel extends Model {
  var flutterLocalNotificationsPlugin;
  String hour = "";
  String minute = "";
  bool mostRecentSearchSuggestionProducts = true;
  bool mostRecentSearchSuggestionStore = true;
  bool status = false;

  void changeNotificationTime(BuildContext context, TimeOfDay newTime) async {
    hour = newTime.hour.toString();
    minute = newTime.minute.toString().length == 1
        ? "0" + newTime.minute.toString()
        : newTime.minute.toString();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('notificationHour', hour);
    await prefs.setString('notificationMinute', minute);
    notifyListeners();

    if (status) {
      turnOffNotifications(context, false);
      turnOnNotification(context);
    }
  }

  void changeNotificationSetting(BuildContext context) async {
    status = !status;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('notificationStatus', status);
    if (status) {
      turnOnNotification(context);
    } else {
      turnOffNotifications(context, true);
    }
    notifyListeners();
  }

  void turnOnNotification(BuildContext context) async {
    var time = new Time(
      int.parse(hour),
      int.parse(minute),
      0,
    );
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'repeatDailyAtTime channel id',
        'repeatDailyAtTime channel name',
        'repeatDailyAtTime description');
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.showDailyAtTime(
        0,
        Translations.textStatic('hbsLong', 'Settings'),
        Translations.textStatic('notificationMessage', 'Settings'),
        time,
        platformChannelSpecifics);

    if (Platform.isAndroid) {
      Toast.show(
          Translations.textStatic('notificationsTurnedOnAt', 'Settings') +
              "$hour:$minute",
          context,
          duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM);
    }
  }

  void turnOffNotifications(BuildContext context, bool showToast) async {
    await flutterLocalNotificationsPlugin.cancelAll();

    if (showToast) {
      Toast.show(
          Translations.textStatic('notificationsOff', 'Settings'), context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
  }

  void loadSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    hour = prefs.getString('notificationHour') ?? "20";
    minute = prefs.getString('notificationMinute') ?? "00";
    status = prefs.getBool('notificationStatus') ?? false;

    notifyListeners();
  }

  void initNotificationPlugin() {
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
    );
  }

  void changeSearchSuggestionTypeProducts() {
    mostRecentSearchSuggestionProducts = !mostRecentSearchSuggestionProducts;
    notifyListeners();
  }

  void changeSearchSuggestionTypeStore() {
    mostRecentSearchSuggestionStore = !mostRecentSearchSuggestionStore;
    notifyListeners();
  }

  static SettingsModel of(BuildContext context) =>
      ScopedModel.of<SettingsModel>(context);
}
