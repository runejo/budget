import 'dart:async';
import 'package:budget_onderzoek/database/datamodels/sync.dart';
import 'package:budget_onderzoek/database/datamodels/sync_db.dart';
import 'package:budget_onderzoek/database/datamodels/search_suggestion_db.dart';
import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:uuid/uuid.dart';
import 'package:budget_onderzoek/translations/translations.dart';

class NewTransactionModel extends Model {
  bool abroadExpense = false;
  String date = DateFormat('yyyy-MM-dd').format(DateTime.now());
  double discountAmount = 0;
  double discountPercentage = 0;
  String discountText = Translations.textStatic('discount', 'Manual_Entry');
  bool isDiscountItem = false;
  String itemMultiplier = "1";
  Map<String, String> modifyingProductGroup;
  String modifyTransactionID;
  bool onlineExpense = false;
  String price = "";
  String product = "";
  String productCategory = "";
  int productCounter = 0;
  bool productInfoComplete = false;
  List<Map<String, String>> products = List<Map<String, String>>();
  bool receiptComplete = false;
  String receiptLocation = "";
  String receiptPrice = "";
  String receiptProductTypeString = "";
  String store = "";
  bool storeInfoComplete = false;
  String storeType = "";
  double totalPrice = 0;

  void changeProductTotal(double valueChange) {
    totalPrice += valueChange * ((100 - discountPercentage) / 100);
    notifyListeners();
  }

  void changeProductDiscountAmount(double valueChange) {
    totalPrice = 0;

    products.forEach((productInfo) {
      totalPrice += double.parse(productInfo['price'].toString());
    });
    discountAmount = valueChange;
    discountPercentage = 0;

    totalPrice = totalPrice - valueChange;
    notifyListeners();
  }

  void changeProductDiscountPercentage(double valueChange) {
    totalPrice = 0;
    products.forEach((productInfo) {
      totalPrice += double.parse(productInfo['price'].toString());
    });
    totalPrice = totalPrice * ((100 - valueChange) / 100);
    discountAmount = 0;
    discountPercentage = valueChange;
    notifyListeners();
  }

  Future<void> addNewTransAction() async {
    if (modifyTransactionID != null) {
      await TransactionDatabase.deleteTransaction(modifyTransactionID);
            notifyListeners();
    }

    // dit moet af en toe gedaan worden, waarom niet hier?
    await TransactionDatabase.deleteBackUp();

    String transactionID =
        modifyTransactionID == null ? Uuid().v1() : modifyTransactionID;

    if (receiptLocation == "") {
      await addManualTransaction(transactionID);
    } else {
      await addReceiptTransaction(transactionID);
    }

    if (modifyTransactionID == null) {
      await SyncDatabase.createSync(RECEIPT_TYPE, transactionID, CREATED);
    } else {
      await SyncDatabase.updateSync(RECEIPT_TYPE, transactionID, UPDATED);
    }

    Synchronise.synchronise();
    notifyListeners();
  }

  Future<void> addReceiptTransaction(String transactionID) async {

    TransactionDatabase trx = TransactionDatabase();
    trx.transactionID = transactionID;
    trx.store = store;
    trx.storeType = storeType;
    trx.date = date;
    trx.totalPrice = double.parse(receiptPrice).toStringAsFixed(2);
    trx.receiptLocation = receiptLocation;

    trx.discountAmount = "0";
    trx.discountPercentage = "0";
    trx.expenseAbroad = "false";
    trx.expenseOnline = "false";
    trx.receiptProductType = receiptProductTypeString;
    TransactionDatabase.insertTransaction(trx);

    addNewProduct();

    products.forEach((productInfo) async {
      TransactionDatabase trx = TransactionDatabase();
      trx.transactionID = transactionID;
      trx.product = productInfo['product'];
      trx.productCategory = productInfo['productCategory'];
      trx.price = productInfo['price'].toString();
      trx.productDate = date;
      trx.productCode = await TransactionDatabase.getProductCode(
          productInfo['productCategory']);
      trx.productGroupID = productInfo['productGroupID'];
      TransactionDatabase.insertProduct(trx);
    });
  }

  Future<void> addManualTransaction(String transactionID) async {

    TransactionDatabase trx = TransactionDatabase();
    trx.transactionID = transactionID;
    trx.store = store;
    trx.storeType = storeType;
    trx.date = date;
    trx.discountAmount = discountAmount.toStringAsFixed(2);
    trx.discountPercentage = discountPercentage.toStringAsFixed(0);
    trx.expenseAbroad = abroadExpense ? "true" : "false";
    trx.expenseOnline = onlineExpense ? "true" : "false";
    trx.totalPrice = totalPrice.toStringAsFixed(2);
    trx.discountText = discountText;
    TransactionDatabase.insertTransaction(trx);

    products.forEach((productInfo) async {
      TransactionDatabase trx = TransactionDatabase();
      trx.transactionID = transactionID;
      trx.product = productInfo['product'];
      trx.productCategory = productInfo['productCategory'];
      trx.price = productInfo['price'].toString();
      trx.productDate = date;
      trx.productCode = await TransactionDatabase.getProductCode(
          productInfo['productCategory']);
      trx.productGroupID = productInfo['productGroupID'];
      TransactionDatabase.insertProduct(trx);
    });
  }

  void isReceiptInfoComplete() {
    if (store != "" && receiptPrice != "" && receiptProductTypeString != "") {
      receiptComplete = true;
    } else {
      receiptComplete = false;
    }
    notifyListeners();
  }

  void isStoreInfoComplete() {
    if (store != "") {
      storeInfoComplete = true;
    } else {
      storeInfoComplete = false;
    }
    notifyListeners();
  }

  void isProductInfoComplete() {
    isStoreInfoComplete();
    if (product != "" && productCategory != "" && price != "") {
      productInfoComplete = true;
    } else {
      productInfoComplete = false;
    }
    notifyListeners();
  }

  void addNewProduct({String oldProductGroupId}) async {
    int itemCount = int.parse(itemMultiplier);
    String productGroupId =
        oldProductGroupId == null ? Uuid().v1() : oldProductGroupId;

    //print(2352345345);
    //print(product);
    //print(productCategory);
    //print(price);
    //Remove from product list
    if (modifyingProductGroup != null) {
      await deleteProduct(modifyingProductGroup);
    }

    //Add to products list
    for (int i = 1; i <= itemCount; i++) {
      productCounter += 1;
      changeProductTotal(double.parse(price));
      products.add({
        "product": product,
        "productCategory": productCategory,
        "price": price,
        "productGroupID": productGroupId,
      });
    }

    //Add to search suggestions table
    String productCode =
        await TransactionDatabase.getProductCode(productCategory);
    SearchSuggestions.addProduct(
        product, productCategory, productCode, itemCount);

    //Reset variables for next dialog
    modifyingProductGroup = null;
    itemMultiplier = "1";
    product = "";
    productCategory = "";
    price = "";
    productInfoComplete = false;
    notifyListeners();
  }

  Future<void> deleteProduct(Map<String, String> productInfo) async {
    changeProductTotal(
        double.parse(productInfo['totalPrice'].toString()) * -1.0);
    productCounter -= int.parse(productInfo['itemCount'].toString());
    products.removeWhere((product) {
      return product['productGroupID'] == productInfo['productGroupID'];
    });
    notifyListeners();

    String productCode = await TransactionDatabase.getProductCode(
        productInfo['productCategory']);
    await SearchSuggestions.removeProduct(
        productCode, int.parse(productInfo['itemCount']));
  }

  Future modifyTransaction(String transactionID) async {
    reset();
    storeInfoComplete = true;
    productInfoComplete = true;
    receiptComplete = true;
    await TransactionDatabase.getTransactionInfo(transactionID, this);
    modifyTransactionID = transactionID;
    notifyListeners();
  }

  Future reset() async {
    store = "";
    storeType = "";
    date = DateFormat('yyyy-MM-dd').format(DateTime.now());
    abroadExpense = false;
    onlineExpense = false;
    product = "";
    productCategory = "";
    price = "";
    totalPrice = 0;
    products = List<Map<String, String>>();
    storeInfoComplete = false;
    productInfoComplete = false;
    productCounter = 0;

    discountPercentage = 0;
    discountAmount = 0;
    discountText = Translations.textStatic('discount', 'Manual_Entry');
    itemMultiplier = "1";

    receiptPrice = "";
    receiptLocation = "";
    receiptComplete = false;
    modifyTransactionID = null;
    isDiscountItem = false;

    receiptProductTypeString = "";
    modifyingProductGroup = null;
  }

  void addItemMultiplier() {
    int itemCount = int.parse(itemMultiplier);
    itemCount = itemCount + 1;
    itemMultiplier = itemCount.toString();
    notifyListeners();
  }

  void subtractItemMultiplier() {
    int itemCount = int.parse(itemMultiplier);
    if (itemCount > 1) {
      itemCount = itemCount - 1;
      itemMultiplier = itemCount.toString();
      notifyListeners();
    }
  }

  void updateReceiptProductType(
      String textUI, String mainProductCategory) async {
    String mainCategory =
        await TransactionDatabase.lookupMainCategoryReceiptType(
            mainProductCategory);

    product = mainCategory;
    productCategory = mainCategory;
    receiptProductTypeString = textUI.replaceAll('\n', ' ');
    isReceiptInfoComplete();
    notifyListeners();
  }

  void reloadProductGroup(Map<String, String> productInfo) {
    product = productInfo['product'];
    productCategory = productInfo['productCategory'];
    price = productInfo['itemPrice'].toString();
    itemMultiplier = productInfo['itemCount'];
    isDiscountItem = double.parse(productInfo['itemPrice']) < 0;
    productInfoComplete = true;
    notifyListeners();
  }

  void modifyProductGroup(Map<String, String> productInfo) {
    modifyingProductGroup = productInfo;
    reloadProductGroup(productInfo);
  }

  void duplicateProductGroup(Map<String, String> productInfo) {
    reloadProductGroup(productInfo);
    addNewProduct();
    notifyListeners();
  }

  static NewTransactionModel of(BuildContext context) =>
      ScopedModel.of<NewTransactionModel>(context);
}
