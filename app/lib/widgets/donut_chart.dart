import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:budget_onderzoek/util/responsive_ui.dart';


class DonutChart extends StatelessWidget {
  DonutChart(this.seriesList, this.changeCategory, {this.animate});

  final bool animate;
  final List<charts.Series> seriesList;

  final Function(String code) changeCategory;

  @override
  Widget build(BuildContext context) {
 
    _onSelectionChanged(charts.SelectionModel model) {
      if (model.selectedDatum.isNotEmpty) {
        changeCategory(model.selectedDatum[0].datum.snapshotCode);
      }
    }

    return charts.PieChart(
      seriesList,
      animate: animate,
      defaultRenderer: charts.ArcRendererConfig(
        arcWidth: (30 * x).floor(),
        arcRendererDecorators: [
          charts.ArcLabelDecorator(
              labelPosition: charts.ArcLabelPosition.outside),
        ],
      ),
      selectionModels: [
        new charts.SelectionModelConfig(
            type: charts.SelectionModelType.info, changedListener: _onSelectionChanged)
      ],
    );
  }
}
