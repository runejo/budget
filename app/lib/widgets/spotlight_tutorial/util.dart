import 'package:budget_onderzoek/widgets/spotlight_tutorial/target_focus.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/target_position.dart';
import 'package:flutter/widgets.dart';


TargetPosition getTargetCurrent(TargetFocus target) {
  if (target.keyTarget != null) {
    var key = target.keyTarget;

    try {
      final RenderBox renderBoxRed = key.currentContext.findRenderObject();
      final size = renderBoxRed.size;
      final offset = renderBoxRed.localToGlobal(Offset.zero);

      return TargetPosition(size, offset);
    } catch (e) {
      print("ERROR: There is not key information for this tutorial screen");
      return null;
    }
  } else {
    return target.targetPosition;
  }
}
