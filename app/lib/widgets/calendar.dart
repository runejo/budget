
import 'package:budget_onderzoek/pages/confirm_day.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:flutter/material.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';


String year, month;

class CalanderWidget extends StatefulWidget {
  CalanderWidget(this._displayYear, this._displayMonth, this._datesOfExperiment,
      this._daysCompleted, this._daysMissing);

  List<DateTime> _datesOfExperiment;
  List<DateTime> _daysCompleted;
  List<DateTime> _daysMissing;
  final int _displayMonth;
  final int _displayYear;
  List<DateTime> _today = [DateTime.now()];

  @override
  State<StatefulWidget> createState() {
    return _CalanderWidgetState();
  }
}

class _CalanderWidgetState extends State<CalanderWidget> {
  List<Map> calendarValues;

  void computeCalendarValues() {
    calendarValues = <Map>[];

    int _firstWeekDayDisplayMonth =
        DateTime(widget._displayYear, widget._displayMonth, 1).weekday;
    int _numberOfDaysDisplayMonth =
        DateTime(widget._displayYear, widget._displayMonth + 1, 0).day;
    int _numberOfDaysPreviousMonth =
        DateTime(widget._displayYear, widget._displayMonth, 0).day;

    //Makes sure to check for next year
    int _previousMonth =
        widget._displayMonth == 1 ? 12 : widget._displayMonth - 1;
    int _nextMonth = widget._displayMonth == 12 ? 1 : widget._displayMonth + 1;

    //Makes sure to check for previous year
    int _previousMonthsYear = widget._displayMonth == 1
        ? widget._displayYear - 1
        : widget._displayYear;
    int _nextMonthsYear = widget._displayMonth == 12
        ? widget._displayYear + 1
        : widget._displayYear;

    //Adds the days to be displayed from the previous month (and formatting value) to the calendarValues list
    for (int i = 0; i < _firstWeekDayDisplayMonth - 1; i++) {
      var calendarValue = Map();
      calendarValue["textValue"] = _numberOfDaysPreviousMonth.toString();
      calendarValue["isTitle"] = false;
      calendarValue["greyValue"] = true;
      addFomatting(_previousMonthsYear, _previousMonth,
          _numberOfDaysPreviousMonth, calendarValue);
      calendarValues.add(calendarValue);
      _numberOfDaysPreviousMonth--;
    }
    calendarValues = calendarValues.reversed.toList();

    //Adds the days to be displayed from the this month (and formatting value) to the calendarValues list
    for (int i = 1; i <= _numberOfDaysDisplayMonth; i++) {
      var calendarValue = Map();
      calendarValue["textValue"] = i.toString();
      calendarValue["isTitle"] = false;
      calendarValue["greyValue"] = false;
      addFomatting(widget._displayYear, widget._displayMonth, i, calendarValue);
      calendarValues.add(calendarValue);
    }

    //Adds the days to be displayed for the next month (and formatting value) to the calanderValues list
    int i = 1;
    while (calendarValues.length < 42) {
      var calendarValue = Map();
      calendarValue["textValue"] = i.toString();
      calendarValue["isTitle"] = false;
      calendarValue["greyValue"] = true;
      addFomatting(_nextMonthsYear, _nextMonth, i, calendarValue);
      calendarValues.add(calendarValue);
      i++;
    }
  }

  void addFomatting(int year, int month, int day, Map calendarValue) {
    DateTime _firstDayOfExperiment = widget._datesOfExperiment[0];
    DateTime _lastDayOfExperiment =
        widget._datesOfExperiment[widget._datesOfExperiment.length - 1];
    calendarValue["firstDayOfExperiment"] = false;
    calendarValue["lastDayOfExperiment"] = false;
    calendarValue["year"] = year;
    calendarValue["month"] = month;
    calendarValue["day"] = day;

    if (_firstDayOfExperiment.year == year &&
        _firstDayOfExperiment.month == month &&
        _firstDayOfExperiment.day == day) {
      calendarValue["firstDayOfExperiment"] = true;
    }
    if (_lastDayOfExperiment.year == year &&
        _lastDayOfExperiment.month == month &&
        _lastDayOfExperiment.day == day) {
      calendarValue["lastDayOfExperiment"] = true;
    }

    calendarValue["inExperiment"] = false;
    for (DateTime specialDate in widget._datesOfExperiment) {
      if (specialDate.year == year &&
          specialDate.month == month &&
          specialDate.day == day) {
        calendarValue["inExperiment"] = true;
      }
    }

    calendarValue["isCompleted"] = false;
    for (DateTime specialDate in widget._daysCompleted) {
      if (specialDate.year == year &&
          specialDate.month == month &&
          specialDate.day == day) {
        calendarValue["isCompleted"] = true;
      }
    }

    calendarValue["isMissing"] = false;
    for (DateTime specialDate in widget._daysMissing) {
      if (specialDate.year == year &&
          specialDate.month == month &&
          specialDate.day == day) {
        calendarValue["isMissing"] = true;
      }
    }

    calendarValue["isToday"] = false;
    for (DateTime specialDate in widget._today) {
      if (specialDate.year == year &&
          specialDate.month == month &&
          specialDate.day == day) {
        calendarValue["isToday"] = true;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Calendar');
    computeCalendarValues();
  
    year = widget._displayYear.toString();
    month = widget._displayMonth.toString();
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 7.0 * x),
      decoration: new BoxDecoration(
        color: Colors.white,
        boxShadow: [
          new BoxShadow(
              color: ColorPallet.veryLightGray,
              offset: new Offset(5.0 * x, 1.0 * x),
              blurRadius: 2.0 * x,
              spreadRadius: 3.0 * x)
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _CalenderColumn([
                {
                  "textValue": translations.text ('monday').substring(0,2),
                  "firstDayOfExperiment": false,
                  "lastDayOfExperiment": false,
                  "isTitle": true,
                  "greyValue": false,
                  "inExperiment": false,
                  "isCompleted": false,
                  "isMissing": false,
                  "isToday": false
                },
                calendarValues[0],
                calendarValues[7],
                calendarValues[14],
                calendarValues[21],
                calendarValues[28],
                calendarValues[35]
              ]),
              _CalenderColumn([
                {
                  "textValue": translations.text ('tuesday').substring(0,2),
                  "firstDayOfExperiment": false,
                  "lastDayOfExperiment": false,
                  "isTitle": true,
                  "greyValue": false,
                  "inExperiment": false,
                  "isCompleted": false,
                  "isMissing": false,
                  "isToday": false
                },
                calendarValues[1],
                calendarValues[8],
                calendarValues[15],
                calendarValues[22],
                calendarValues[29],
                calendarValues[36]
              ]),
              _CalenderColumn([
                {
                  "textValue": translations.text ('wednesday').substring(0,2),
                  "firstDayOfExperiment": false,
                  "lastDayOfExperiment": false,
                  "isTitle": true,
                  "greyValue": false,
                  "inExperiment": false,
                  "isCompleted": false,
                  "isMissing": false,
                  "isToday": false
                },
                calendarValues[2],
                calendarValues[9],
                calendarValues[16],
                calendarValues[23],
                calendarValues[30],
                calendarValues[37]
              ]),
              _CalenderColumn([
                {
                  "textValue": translations.text ('thursday').substring(0,2),
                  "firstDayOfExperiment": false,
                  "lastDayOfExperiment": false,
                  "isTitle": true,
                  "greyValue": false,
                  "inExperiment": false,
                  "isCompleted": false,
                  "isMissing": false,
                  "isToday": false
                },
                calendarValues[3],
                calendarValues[10],
                calendarValues[17],
                calendarValues[24],
                calendarValues[31],
                calendarValues[38]
              ]),
              _CalenderColumn([
                {
                  "textValue": translations.text ('friday').substring(0,2),
                  "firstDayOfExperiment": false,
                  "lastDayOfExperiment": false,
                  "isTitle": true,
                  "greyValue": false,
                  "inExperiment": false,
                  "isCompleted": false,
                  "isMissing": false,
                  "isToday": false
                },
                calendarValues[4],
                calendarValues[11],
                calendarValues[18],
                calendarValues[25],
                calendarValues[32],
                calendarValues[39]
              ]),
              _CalenderColumn([
                {
                  "textValue": translations.text ('saturday').substring(0,2),
                  "firstDayOfExperiment": false,
                  "lastDayOfExperiment": false,
                  "isTitle": true,
                  "greyValue": false,
                  "inExperiment": false,
                  "isCompleted": false,
                  "isMissing": false,
                  "isToday": false
                },
                calendarValues[5],
                calendarValues[12],
                calendarValues[19],
                calendarValues[26],
                calendarValues[33],
                calendarValues[40]
              ]),
              _CalenderColumn([
                {
                  "textValue": translations.text ('sunday').substring(0,2),
                  "firstDayOfExperiment": false,
                  "lastDayOfExperiment": false,
                  "isTitle": true,
                  "greyValue": false,
                  "inExperiment": false,
                  "isCompleted": false,
                  "isMissing": false,
                  "isToday": false
                },
                calendarValues[6],
                calendarValues[13],
                calendarValues[20],
                calendarValues[27],
                calendarValues[34],
                calendarValues[41]
              ]),
            ],
          )
        ],
      ),
    );
  }
}

//This widget generates a single column of text widgets, to be used in the calander widget.
//This widget requires a list of maps: textValue, greyValue, experimentValue, pastValue, completedValue, todayValue
//The text value determines the text, the bool value determines whether the text should be displayed in bold.
class _CalenderColumn extends StatelessWidget {
  _CalenderColumn(this.columnValues);

  List<Map> columnValues;

  List<Color> getBackgroundColor(Map value) {
    if (value["firstDayOfExperiment"]) {
      return [ColorPallet.veryLightBlue, Colors.transparent];
    } else if (value["lastDayOfExperiment"]) {
      return [Colors.transparent, ColorPallet.veryLightBlue];
    } else if (value["inExperiment"]) {
      return [ColorPallet.veryLightBlue, ColorPallet.veryLightBlue];
    } else {
      return [Colors.transparent, Colors.transparent];
    }
  }

  Color getCircleColor(Map value) {
    if (value["isCompleted"]) {
      return ColorPallet.lightGreen;
    } else if (value["isToday"]) {
      return ColorPallet.primaryColor;
    } else if (value["isMissing"]) {
      return ColorPallet.orange;
    } else if (value["inExperiment"]) {
      return ColorPallet.veryLightBlue;
    } else {
      return Colors.transparent;
    }
  }

  Color getTextColor(Map value) {
    if (value["isCompleted"] && value["isToday"]) {
      return Colors.white;
    }
    if (value["greyValue"] && (value["isCompleted"] || value["isMissing"])) {
      return ColorPallet.veryLightGray;
    } else if (value["isToday"]) {
      return ColorPallet.darkTextColor;
    } else if (value["greyValue"]) {
      return ColorPallet.midGray;
    } else {
      return ColorPallet.darkTextColor;
    }
  }

  @override
  Widget build(BuildContext context) {
    final textWidgets = columnValues.map((Map value) {
      return Container(
        width: 56.0 * x,
        decoration: value["isTitle"]
            ? BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: ColorPallet.lightGray),
                ),
              )
            : BoxDecoration(),
        child: Container(
          width: 30.0 * x,
          height: 38.0 * y,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: getBackgroundColor(value),
                begin: Alignment.centerRight,
                end: Alignment.centerLeft,
                tileMode: TileMode.clamp,
                stops: [0.5, 0.5]),
          ),
          child: InkWell(
            onTap: () {
              if (value["isTitle"] == false) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ConfirmDayPage(value)));
              }
            },
            child: Container(
              height: 30.0 * y,
              width: 30.0 * x,
              margin: EdgeInsets.symmetric(vertical: 1.0 * y),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: getCircleColor(value),
              ),
              child: Center(
                child: Text(
                  value["textValue"],
                  style: TextStyle(
                      fontWeight:
                          value["isTitle"] ? FontWeight.w800 : FontWeight.w700,
                      fontSize: 15.0 * f,
                      color: getTextColor(value)),
                ),
              ),
            ),
          ),
        ),
      );
    }).toList();

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: textWidgets,
    );
  }
}
