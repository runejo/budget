import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:budget_onderzoek/util/date_string.dart';
import 'dart:math';
import 'package:budget_onderzoek/util/responsive_ui.dart';

final charts.Color myBlue = charts.Color(
    r: ColorPallet.primaryColor.red,
    g: ColorPallet.primaryColor.green,
    b: ColorPallet.primaryColor.blue,
    a: ColorPallet.primaryColor.alpha);

final charts.Color myGreen = charts.Color(
    r: ColorPallet.lightGreen.red,
    g: ColorPallet.lightGreen.green,
    b: ColorPallet.lightGreen.blue,
    a: ColorPallet.lightGreen.alpha);

final charts.Color myOrange = charts.Color(
    r: ColorPallet.orange.red,
    g: ColorPallet.orange.green,
    b: ColorPallet.orange.blue,
    a: ColorPallet.orange.alpha);

final charts.Color myPink = charts.Color(
    r: ColorPallet.pink.red,
    g: ColorPallet.pink.green,
    b: ColorPallet.pink.blue,
    a: ColorPallet.pink.alpha);

final charts.Color myDarkBlue = charts.Color(
    r: ColorPallet.darkTextColor.red,
    g: ColorPallet.darkTextColor.green,
    b: ColorPallet.darkTextColor.blue,
    a: ColorPallet.darkTextColor.alpha);


class BarChart extends StatelessWidget {
  BarChart(this.seriesList, this.title, this.staticTicks, this.changePeriod,
      this.week, this.absMinDate, this.absMaxDate,
      {this.animate});

  final int absMaxDate;
  final int absMinDate;
  final bool animate;
  final List<charts.Series> seriesList;
  final List<charts.TickSpec<String>> staticTicks;
  final String title;
  final bool week;

  final Function(DateTime _dayInPeriod, bool toWeek) changePeriod;

  @override
  Widget build(BuildContext context) {


    final customTickFormatter = charts.BasicNumericTickFormatterSpec(
        (num value) => '€' + value.round().toString());

    _onSelectionChanged(charts.SelectionModel model) {
      if (model.selectedDatum.isNotEmpty) {
        DateTime parsedDate = DateTime.parse(model.selectedDatum[0].datum.date);
        int newIntDate = DateString.dateTimeToDateInt(parsedDate);
        newIntDate = max(newIntDate, absMinDate);
        newIntDate = min(newIntDate, absMaxDate);
        parsedDate = DateString.dateIntToDateTime(newIntDate);
        changePeriod(parsedDate, true);
      
      }
    }

    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 5),
          child: charts.BarChart(
            seriesList,
            animate: animate,
            barGroupingType: charts.BarGroupingType.stacked,
            primaryMeasureAxis: charts.NumericAxisSpec(
              renderSpec: charts.GridlineRendererSpec(
                  labelStyle: charts.TextStyleSpec(
                      color: myDarkBlue,
                      fontFamily: 'Source Sans Pro Semibold',
                      fontSize: (14.0 * f).floor())),
              showAxisLine: false,
              tickFormatterSpec: customTickFormatter,
              tickProviderSpec: charts.BasicNumericTickProviderSpec(
                desiredMinTickCount: 4,
              ),
            ),
            domainAxis: charts.OrdinalAxisSpec(
              tickProviderSpec:
                  new charts.StaticOrdinalTickProviderSpec(staticTicks),
              
              renderSpec: charts.SmallTickRendererSpec(
                labelStyle: charts.TextStyleSpec(
                    fontFamily: 'Source Sans Pro Semibold',
                    color: myDarkBlue,
                    fontSize: (14.0 * f).floor()),
              ),
            ),
           
            selectionModels: [
              charts.SelectionModelConfig(
                type: charts.SelectionModelType.info,
                changedListener: _onSelectionChanged,
              )
            ],
          ),
        ),
        
      ],
    );
  }
}

class ExpenseDate {
  ExpenseDate(this.date, this.expense);

  final String date;
  final int expense;
}
