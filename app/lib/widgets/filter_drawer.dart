import 'dart:async';

import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/scoped_models/fliter_scoped_model.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRangePicker;
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';


Translations translations;

class FliterDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Filter');
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: 50.0 * y,
                  color: ColorPallet.primaryColor,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(width: 25.0 * x),
                      Text(
                        translations.text ('filterExpenses'),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22.0 * f,
                            fontWeight: FontWeight.w600),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      InkWell(
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 8.0 * x, top: 8 * y, bottom: 8 * y),
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 30.0 * x,
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(width: 25.0 * x),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                _DateFilterWidget(),
                _PriceRangeFilterWidget(),
              ],
            ),
          ),
          SizedBox(
            height: 15.0 * y,
          ),
          ScopedModelDescendant<FilterModel>(
            builder: (context, child, model) => Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  textColor: Colors.white,
                  onPressed: () {
                    FilterModel.of(context).reset();
                  },
                  child: Text(
                    translations.text ('removeFilters'),
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: 16 * f),
                  ),
                  color: model.endDate == "" &&
                          model.startDate == "" &&
                          model.minimumPrice == "" &&
                          model.maximumPrice == ""
                      ? ColorPallet.midGray
                      : ColorPallet.primaryColor,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 25.0 * y,
          ),
        ],
      ),
    );
  }
}

class _DateFilterWidget extends StatelessWidget {
  Future<String> getStartDate() async {
    var dbStartValue = await TransactionDatabase.getFirstAndLastDate();
    return dbStartValue['first'];
  }

  Future<String> getEndDate() async {
    var dbEndValue = await TransactionDatabase.getFirstAndLastDate();
    return dbEndValue['last'];
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        Map<String, dynamic> dateRange =
            await TransactionDatabase.getFirstAndLastDate();
        final List<DateTime> picked = await DateRangePicker.showDatePicker(
            locale: International.languageFromId(LanguageSetting.key).locale,
            context: context,
            initialFirstDate: DateTime.parse(dateRange['first']),
            initialLastDate: DateTime.parse(dateRange['last']),
            firstDate: DateTime(2019),
            lastDate: DateTime(2020));
        if (picked != null && picked.length == 2) {
          FilterModel.of(context).startDate =
              DateFormat('yyyy-MM-dd').format(picked[0]);
          FilterModel.of(context).endDate =
              DateFormat('yyyy-MM-dd').format(picked[1]);
          FilterModel.of(context).notifyListeners();
        }
      },
      child: ScopedModelDescendant<FilterModel>(
        builder: (context, child, model) => ExpansionTile(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(translations.text ('date'),
                  style: TextStyle(
                      color: ColorPallet.darkTextColor,
                      fontSize: 18.0 * f,
                      fontWeight: FontWeight.w600)),
            ],
          ),
          leading: Icon(Icons.calendar_today,
              color: ColorPallet.darkTextColor, size: 24.0 * x),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16.0 * x, vertical: 16.0 * y),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text ('beginDate'),
                      style: TextStyle(
                          color: ColorPallet.darkTextColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 16 * f)),
                  FutureBuilder(
                      future: getStartDate(),
                      builder: (BuildContext context,
                          AsyncSnapshot<String> snapshot) {
                        if (!snapshot.hasData || snapshot.data.length == 0) {
                          return Container();
                        }
                        return Text(
                            DateFormat(
                                    'E, d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
                                .format(DateTime.parse(model.startDate != ""
                                    ? model.startDate
                                    : snapshot.data)),
                            style: TextStyle(
                                color: model.startDate != snapshot.data &&
                                        model.startDate != ""
                                    ? ColorPallet.primaryColor
                                    : ColorPallet.midGray,
                                fontWeight: FontWeight.w600,
                                fontSize: 16 * f));
                      }),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16.0 * x, vertical: 16.0 * y),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text ('endDate'),
                      style: TextStyle(
                          color: ColorPallet.darkTextColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 16 * f)),
                  FutureBuilder(
                      future: getEndDate(),
                      builder: (BuildContext context,
                          AsyncSnapshot<String> snapshot) {
                        if (!snapshot.hasData || snapshot.data.length == 0) {
                          return Container();
                        }
                        return Text(
                            DateFormat(
                                    'E, d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
                                .format(DateTime.parse(model.endDate != ""
                                    ? model.endDate
                                    : snapshot.data)),
                            style: TextStyle(
                                color: model.endDate != snapshot.data &&
                                        model.endDate != ""
                                    ? ColorPallet.primaryColor
                                    : ColorPallet.midGray,
                                fontWeight: FontWeight.w600,
                                fontSize: 16 * f));
                      }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TextEditingControllerWorkaroud extends TextEditingController {
  TextEditingControllerWorkaroud({String text}) : super(text: text);

  void setTextAndPosition(String newText, {int caretPosition}) {
    int offset = caretPosition != null ? caretPosition : newText.length;
    value = value.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: offset),
        composing: TextRange.empty);
  }
}

class _PriceRangeFilterWidget extends StatefulWidget {
  @override
  _PriceRangeFilterWidgetState createState() => _PriceRangeFilterWidgetState();
}

class _PriceRangeFilterWidgetState extends State<_PriceRangeFilterWidget> {
  Map<String, dynamic> hintText;
  String maxPrice = "";
  TextEditingControllerWorkaroud maxPriceController =
      TextEditingControllerWorkaroud();

  String minPrice = "";
  TextEditingControllerWorkaroud minPriceController =
      TextEditingControllerWorkaroud();

  void checkUserInput(String newPrice, BuildContext context, bool isMinPrice) {
    if (!UserInput.intValidator(newPrice)) {
      Toast.show(translations.text ('invalidAmount'), context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);

      if (isMinPrice) {
        minPriceController.setTextAndPosition(minPrice);
      } else {
        maxPriceController.setTextAndPosition(maxPrice);
      }
    } else {
      if (isMinPrice) {
        minPrice = newPrice;
      } else {
        maxPrice = newPrice;
      }
    }
  }

  Future<String> getMinimumValue() async {
    var dbMinValue = await TransactionDatabase.getLowestAndHighestValue();
    return dbMinValue['lowest'];
  }

  Future<String> getMaximumValue() async {
    var dbHighValue = await TransactionDatabase.getLowestAndHighestValue();
    return dbHighValue['highest'];
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        minPriceController.text = "";
        maxPriceController.text = "";

        hintText = await TransactionDatabase.getLowestAndHighestValue();

        if (FilterModel.of(context).minimumPrice != "") {
          hintText['lowest'] = FilterModel.of(context).minimumPrice;
        }

        if (FilterModel.of(context).maximumPrice != "") {
          hintText['highest'] = FilterModel.of(context).maximumPrice;
        }

        minPrice = hintText['lowest'];
        maxPrice = hintText['highest'];

        await showDialog(
            context: context,
            builder: (BuildContext context) {
              return SimpleDialog(
                contentPadding: EdgeInsets.all(0),
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        color: ColorPallet.primaryColor,
                        height: 68,
                        width: 400,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 20),
                            Padding(
                              padding: EdgeInsets.only(left: 35.0),
                              child: Text(translations.text ('filterAmount'),
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(bottom: 20),
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 60,
                              width: 400,
                            ),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        "Min",
                                        style: TextStyle(
                                            color: ColorPallet.primaryColor,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 21),
                                      ),
                                      Container(
                                        width: 80,
                                        height: 40,
                                        child: TextField(
                                          textAlign: TextAlign.center,
                                          controller: minPriceController,
                                          style: TextStyle(
                                              color: ColorPallet.darkTextColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18),
                                          autocorrect: false,
                                          keyboardType:
                                              TextInputType.numberWithOptions(
                                                  decimal: true),
                                          onChanged: (newPrice) {
                                            setState(() {
                                              checkUserInput(
                                                  newPrice, context, true);
                                            });
                                          },
                                          onSubmitted: (value) {},
                                          decoration: InputDecoration(
                                            hintText: hintText['lowest'],
                                            hintStyle: TextStyle(
                                                color: ColorPallet.midGray,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 18),
                                            filled: true,
                                            fillColor: Colors.transparent,
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(width: 80),
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        "Max",
                                        style: TextStyle(
                                            color: ColorPallet.primaryColor,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 21),
                                      ),
                                      Container(
                                        width: 80,
                                        height: 40,
                                        child: TextField(
                                          textAlign: TextAlign.center,
                                          controller: maxPriceController,
                                          style: TextStyle(
                                              color: ColorPallet.darkTextColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18),
                                          autocorrect: false,
                                          keyboardType:
                                              TextInputType.numberWithOptions(
                                                  decimal: true),
                                          onChanged: (newPrice) {
                                            setState(() {
                                              checkUserInput(
                                                  newPrice, context, false);
                                            });
                                          },
                                          onSubmitted: (value) {},
                                          decoration: InputDecoration(
                                            hintText: hintText['highest'],
                                            hintStyle: TextStyle(
                                                color: ColorPallet.midGray,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 18),
                                            filled: true,
                                            fillColor: Colors.transparent,
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ]),
                            SizedBox(height: 60),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  FlatButton(
                                    child: Text(
                                      translations.text ('cancel').toUpperCase(),
                                      style: TextStyle(
                                          color: ColorPallet.primaryColor),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                  FlatButton(
                                    child: Text(
                                      translations.text ('ok').toUpperCase(),
                                      style: TextStyle(
                                          color: ColorPallet.primaryColor),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                      FilterModel.of(context).minimumPrice =
                                          minPrice;
                                      FilterModel.of(context).maximumPrice =
                                          maxPrice;
                                      FilterModel.of(context).notifyListeners();
                                    },
                                  )
                                ]),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              );
            });
      },
      child: ScopedModelDescendant<FilterModel>(
        builder: (context, child, model) => ExpansionTile(
          title: Text(translations.text ('priceRange'),
              style: TextStyle(
                  color: ColorPallet.darkTextColor,
                  fontSize: 18.0 * f,
                  fontWeight: FontWeight.w600)),
          leading: Icon(Icons.euro_symbol,
              color: ColorPallet.darkTextColor, size: 24.0 * x),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16.0 * x, vertical: 16.0 * y),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    translations.text ('minimum'),
                    style: TextStyle(
                        color: ColorPallet.darkTextColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 16 * f),
                  ),
                  FutureBuilder<String>(
                    future: getMinimumValue(),
                    builder:
                        (BuildContext context, AsyncSnapshot<String> snapshot) {
                      if (!snapshot.hasData || snapshot.data.length == 0) {
                        return Container();
                      }
                      return Text(
                        model.minimumPrice != ""
                            ? model.minimumPrice
                            : snapshot.data,
                        style: TextStyle(
                            color: model.minimumPrice != snapshot.data &&
                                    model.minimumPrice != ""
                                ? ColorPallet.primaryColor
                                : ColorPallet.midGray,
                            fontWeight: FontWeight.w600,
                            fontSize: 16 * f),
                      );
                    },
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16.0 * x, vertical: 16.0 * y),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    translations.text ('maximum'),
                    style: TextStyle(
                        color: ColorPallet.darkTextColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 16 * f),
                  ),
                  FutureBuilder<String>(
                    future: getMaximumValue(),
                    builder:
                        (BuildContext context, AsyncSnapshot<String> snapshot) {
                      if (!snapshot.hasData || snapshot.data.length == 0) {
                        return Container();
                      }
                      return Text(
                        model.maximumPrice != ""
                            ? model.maximumPrice
                            : snapshot.data,
                        style: TextStyle(
                            color: model.maximumPrice != snapshot.data &&
                                    model.maximumPrice != ""
                                ? ColorPallet.primaryColor
                                : ColorPallet.midGray,
                            fontWeight: FontWeight.w600,
                            fontSize: 16 * f),
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

