import 'dart:io';
import 'dart:async';
import 'dart:ui';

import 'package:budget_onderzoek/pages/main_expenselist.dart';
import 'package:budget_onderzoek/pages/new_receipt.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:budget_onderzoek/widgets/draggable_scrollbar.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:budget_onderzoek/widgets/custom_flutter/custom_dialog.dart'
    as customDialog;
import 'package:shared_preferences/shared_preferences.dart';

Translations translations;

class CameraPage extends StatefulWidget {
  CameraPage(this.isEditing);

  final bool isEditing;

  @override
  __CameraPageState createState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));
    return __CameraPageState();
  }
}

class __CameraPageState extends State<CameraPage> {
  CameraController _controller;
  String _dirPath;

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    availableCameras().then((cameras) {
      _controller = CameraController(cameras[0], ResolutionPreset.high,
          enableAudio: false);
      _controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    });
    showCameraInstruction();
  }

  void onTakePictureButtonPressed() {
    setState(() {});
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {});
        if (filePath != null) {
          setState(() {});
          print('Picture saved to $filePath');
          NewTransactionModel.of(context).receiptLocation = filePath;

          if (widget.isEditing) {
            Navigator.pop(context);
          }

          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ReceiptDetailsPage(),
            ),
          );
        }
      }
    });
  }

  Future<String> takePicture() async {
    if (!_controller.value.isInitialized) {
      print('Error select camera first');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    _dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(_dirPath).create(recursive: true);
    final String filePath =
        '$_dirPath/${DateTime.now().millisecondsSinceEpoch.toString()}.jpg';
    if (_controller.value.isTakingPicture) {
      // A capture is already pending, do nothing
      return null;
    }

    try {
      await _controller.takePicture(filePath);
    } on CameraException catch (e) {
      print(e);
      return null;
    }
    return filePath;
  }

  void showCameraInstruction() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String status = prefs.getString("cameraTutorial") ?? "";
    if (status == "") {
      prefs.setString("cameraTutorial", "shown");
      new Future.delayed(
        Duration(milliseconds: 500),
        () async {
          await showDialog(
            context: context,
            builder: (BuildContext context) {
              return TipsDialog();
            },
          );
        },
      );
    }
  }

  Widget _cameraPreviewWidget() {
    if (_controller == null || !_controller.value.isInitialized) {
      return Container();
    } else {
      return AspectRatio(
        aspectRatio: _controller.value.aspectRatio,
        child: CameraPreview(_controller),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Camera');
    return WillPopScope(
      onWillPop: () {
        if (widget.isEditing) {
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
            statusBarColor: ColorPallet.pink,
          ));
        } else {
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
            statusBarColor: ColorPallet.primaryColor,
          ));
        }

        showFirstExpenseListTutorial = true;
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorPallet.pink,
          title: Row(
            children: <Widget>[
              Text(
                translations.text('photographReceipt'),
                style: TextStyle(fontSize: 24 * f),
              ),
              Expanded(child: Container()),
              InkWell(
                onTap: () async {
                  await showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return TipsDialog();
                    },
                  );
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 5.0 * x,
                    vertical: 5 * y,
                  ),
                  child: Icon(Icons.info, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
        body: Container(
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: _cameraPreviewWidget(),
                    ),
                  ),
                ],
              ),
              Positioned(
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: double.infinity,
                    ),
                    Positioned(
                      bottom: 20,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 70 * x,
                              height: 70 * y,
                              child: FittedBox(
                                child: FloatingActionButton(
                                  child: Icon(Icons.camera),
                                  backgroundColor: ColorPallet.pink,
                                  onPressed: () {
                                    onTakePictureButtonPressed();
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _ScannerOverlayShape extends ShapeBorder {
  _ScannerOverlayShape({
    this.borderColor = Colors.white,
    this.borderWidth = 1.0,
    this.overlayColor = const Color(0x88000000),
  });

  final Color borderColor;
  final double borderWidth;
  final Color overlayColor;

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.all(10.0);

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) {
    return Path()
      ..fillType = PathFillType.evenOdd
      ..addPath(getOuterPath(rect), Offset.zero);
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    Path _getLeftTopPath(Rect rect) {
      return Path()
        ..moveTo(rect.left, rect.bottom)
        ..lineTo(rect.left, rect.top)
        ..lineTo(rect.right, rect.top);
    }

    return _getLeftTopPath(rect)
      ..lineTo(
        rect.right,
        rect.bottom,
      )
      ..lineTo(
        rect.left,
        rect.bottom,
      )
      ..lineTo(
        rect.left,
        rect.top,
      );
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {
    const lineSize = 30;

    final width = rect.width;
    final borderWidthSize = width * 10 / 100 + 100;
    final height = rect.height - 100;
    final borderHeightSize = height - (width - borderWidthSize);
    final borderSize = Size(borderWidthSize / 2, borderHeightSize / 2);

    var paint = Paint()
      ..color = overlayColor
      ..style = PaintingStyle.fill;

    canvas
      ..drawRect(
        Rect.fromLTRB(
            rect.left, rect.top, rect.right, borderSize.height + rect.top - 50),
        paint,
      )
      ..drawRect(
        Rect.fromLTRB(rect.left, rect.bottom - borderSize.height, rect.right,
            rect.bottom),
        paint,
      )
      ..drawRect(
        Rect.fromLTRB(rect.left, rect.top + borderSize.height - 50,
            rect.left + borderSize.width, rect.bottom - borderSize.height),
        paint,
      )
      ..drawRect(
        Rect.fromLTRB(
            rect.right - borderSize.width,
            rect.top + borderSize.height - 50,
            rect.right,
            rect.bottom - borderSize.height),
        paint,
      );

    paint = Paint()
      ..color = borderColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    final borderOffset = borderWidth / 2;
    final realReact = Rect.fromLTRB(
        borderSize.width + borderOffset, // + 25,
        borderSize.height + borderOffset + rect.top - 50, // - 75,
        width - borderSize.width - borderOffset, // - 25,
        height - borderSize.height - borderOffset + rect.top + 100);

    //Draw top right corner
    canvas
      ..drawPath(
          Path()
            ..moveTo(realReact.right, realReact.top)
            ..lineTo(realReact.right, realReact.top + lineSize),
          paint)
      ..drawPath(
          Path()
            ..moveTo(realReact.right, realReact.top)
            ..lineTo(realReact.right - lineSize, realReact.top),
          paint)
      ..drawPoints(
        PointMode.points,
        [Offset(realReact.right, realReact.top)],
        paint,
      )

      //Draw top left corner
      ..drawPath(
          Path()
            ..moveTo(realReact.left, realReact.top)
            ..lineTo(realReact.left, realReact.top + lineSize),
          paint)
      ..drawPath(
          Path()
            ..moveTo(realReact.left, realReact.top)
            ..lineTo(realReact.left + lineSize, realReact.top),
          paint)
      ..drawPoints(
        PointMode.points,
        [Offset(realReact.left, realReact.top)],
        paint,
      )

      //Draw bottom right corner
      ..drawPath(
          Path()
            ..moveTo(realReact.right, realReact.bottom)
            ..lineTo(realReact.right, realReact.bottom - lineSize),
          paint)
      ..drawPath(
          Path()
            ..moveTo(realReact.right, realReact.bottom)
            ..lineTo(realReact.right - lineSize, realReact.bottom),
          paint)
      ..drawPoints(
        PointMode.points,
        [Offset(realReact.right, realReact.bottom)],
        paint,
      )

      //Draw bottom left corner
      ..drawPath(
          Path()
            ..moveTo(realReact.left, realReact.bottom)
            ..lineTo(realReact.left, realReact.bottom - lineSize),
          paint)
      ..drawPath(
          Path()
            ..moveTo(realReact.left, realReact.bottom)
            ..lineTo(realReact.left + lineSize, realReact.bottom),
          paint)
      ..drawPoints(
        PointMode.points,
        [Offset(realReact.left, realReact.bottom)],
        paint,
      );
  }

  @override
  ShapeBorder scale(double t) {
    return _ScannerOverlayShape(
      borderColor: borderColor,
      borderWidth: borderWidth,
      overlayColor: overlayColor,
    );
  }
}

class TipsDialog extends StatelessWidget {
  Widget tipText(double width, String number, String text) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  number,
                  style: TextStyle(
                    fontSize: 18 * f,
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ],
            ),
            SizedBox(width: 10 * x),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: width - 80 * x,
                  child: Text(
                    text,
                    style: TextStyle(
                      fontSize: 16 * f,
                      color: ColorPallet.darkTextColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox(height: 15 * y),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    ScrollController _arrowsController = ScrollController();
    double width = 360 * x;
    return customDialog.AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      contentPadding: EdgeInsets.all(0),
      content: Container(
        height: 500 * y,
        width: 360 * x,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  width: width,
                  child: Stack(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          SizedBox(height: 30 * y),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.asset(
                                "images/hint_icon_white.png",
                                width: 35 * x,
                              ),
                              SizedBox(width: 12 * x),
                              Text(
                                translations.text('tips'),
                                style: TextStyle(
                                  fontSize: 28 * f,
                                  color: ColorPallet.darkTextColor,
                                  fontWeight: FontWeight.w900,
                                ),
                              ),
                              SizedBox(width: 35 * x),
                            ],
                          ),
                        ],
                      ),
                      Positioned(
                        right: 0,
                        top: 0,
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 18.0 * y,
                              horizontal: 18 * x,
                            ),
                            child: Icon(
                              Icons.close,
                              color: ColorPallet.darkTextColor,
                              size: 27 * x,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 15 * y),
            Container(
              height: 310 * y,
              width: width,
              child: DraggableScrollbar.rrect(
                alwaysVisibleScrollThumb: false,
                heightScrollThumb: 200.0,
                backgroundColor: Colors.grey, //ColorPallet.primaryColor,
                padding: EdgeInsets.only(right: 4.0),
                controller: _arrowsController,
                child: ListView(
                  controller: _arrowsController,
                  scrollDirection: Axis.vertical,
                  children: <Widget>[
                    tipText(width, "1.", translations.text('cameraHint1')),
                    tipText(width, "2.", translations.text('cameraHint2')),
                    tipText(width, "3.", translations.text('cameraHint3')),
                    tipText(width, "4.", translations.text('cameraHint4')),
                    tipText(width, "5.", translations.text('cameraHint5')),
                    tipText(width, "6.", translations.text('cameraHint6')),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: ButtonTheme(
                  minWidth: 160 * x,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Text(
                      translations.text('understood'),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16 * f,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    color: ColorPallet.darkTextColor,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ),
            ),
            SizedBox(height: 15 * y),
          ],
        ),
      ),
    );
  }
}
