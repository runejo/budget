import 'dart:async';
import 'dart:io';
import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:budget_onderzoek/database/datamodels/sync_db.dart';
import 'package:budget_onderzoek/pages/new_receipt.dart';
import 'package:budget_onderzoek/resources/category_icon_map.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/scoped_models/expenselist_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/fliter_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/tutorials/main_expenselist_tutorial.dart';
import 'package:budget_onderzoek/util/group_products.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:budget_onderzoek/widgets/filter_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'new_entry.dart';
import 'package:badges/badges.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';


Translations translations;

String today, yesterday, lastDate;
BuildContext updatedContextRef;
bool showFirstExpenseListTutorial = false;

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();

class TransactionsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TransactionsPageState();
  }
}

class _TransactionsPageState extends State<TransactionsPage> {
  bool searchEnabled = false;
  bool variableExpense = true;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    initTargets(keyButton1, keyButton2, keyButton3);
    super.initState();

    Future.delayed(Duration(milliseconds: 200), () {
      showInitialTutorial(context);
    });
  }

  void enableSearch() {
    setState(() {
      searchEnabled = true;
    });
  }

  void disableSearch() {
    setState(() {
      searchEnabled = false;
      ExpenseListModel.of(context).reset();
    });
  }

  void enableVariableExpense() {
    setState(() {
      variableExpense = true;
    });
  }

  void disableVariableExpense() {
    setState(() {
      variableExpense = false;
    });
  }

  void openFilterDrawer() {
    _scaffoldKey.currentState.openEndDrawer();
  }

  void showInitialTutorial(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String status = prefs.getString("mainExpenselistTutorial") ?? "";
    if (status == "" && showFirstExpenseListTutorial) {
      prefs.setString("mainExpenselistTutorial", "shown");
      showTutorial(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      key: _scaffoldKey,
      endDrawer: Container(
        width: 295.0 * x,
        child: FliterDrawer(),
      ),
      body: Column(
        children: <Widget>[
          _TopBarWidget(
              searchEnabled, enableSearch, disableSearch, openFilterDrawer),
          Expanded(child: _ExpenseListWidget())
        ],
      ),
    );
  }
}

class _TopBarWidget extends StatelessWidget {
  _TopBarWidget(this.searchEnabled, this.enableSearch, this.disableSearch,
      this.openFilterDrawer);

  final bool searchEnabled;

  final Function() enableSearch;

  final Function() disableSearch;

  final Function() openFilterDrawer;

  Widget getFilerIcon(FilterModel model) {
    int filterCounter = 0;
    if (model.endDate != "" || model.startDate != "") {
      filterCounter = filterCounter + 1;
    }
    if (model.minimumPrice != "" || model.maximumPrice != "") {
      filterCounter = filterCounter + 1;
    }

    if (filterCounter == 0) {
      return Icon(Icons.filter_list, size: 37.0 * f, color: Colors.white);
    } else
      return Badge(
        position: BadgePosition(top: 2 * y),
        badgeColor: ColorPallet.pink,
        badgeContent: Text(
          filterCounter.toString(),
          style: TextStyle(color: Colors.white),
        ),
        child: Icon(Icons.filter_list, size: 37.0 * f, color: Colors.white),
      );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> searchEnabledWidgets = [
      SizedBox(width: 22.0 * x),
      Text(
        Translations.textStatic('expensesPage', 'Spending'),
        style: TextStyle(
            color: Colors.white,
            fontSize: 22.0 * f,
            fontWeight: FontWeight.w600),
      ),
      SizedBox(width: 10 * x),
      InkWell(
          key: keyButton1,
          onTap: () {
            showTutorial(context);
          },
          child: Icon(Icons.info, color: Colors.white, size: 28.0 * x)),
      Expanded(child: Container()),
      InkWell(
        child: Icon(Icons.search,
            key: keyButton2, size: 37.0 * f, color: Colors.white),
        onTap: enableSearch,
      ),
      SizedBox(width: 6.0 * x),
      ScopedModelDescendant<FilterModel>(
        builder: (context, child, model) => InkWell(
          key: keyButton3,
          child: getFilerIcon(model),
          onTap: openFilterDrawer,
        ),
      ),
      SizedBox(width: 22.0 * x),
    ];

    List<Widget> searchDisabled = [
      SizedBox(width: 12.0 * x),
      Container(
        width: 350.0 * x,
        child: TextField(
          onChanged: (value) {
            ExpenseListModel.of(context).searchWord = value;
            ExpenseListModel.of(context).notifyListeners();
          },
          autofocus: true,
          style: TextStyle(color: Colors.white, fontSize: 20.0 * f),
          decoration: InputDecoration(
            border: InputBorder.none,
            prefixIcon: Icon(
              Icons.search,
              color: ColorPallet.veryLightBlue,
              size: 24 * x,
            ),
            hintText: Translations.textStatic('findTransactions', 'Manual_Entry'),
            hintStyle:
                TextStyle(color: ColorPallet.veryLightBlue, fontSize: 18.0),
          ),
        ),
      ),
      InkWell(
        child: Icon(
          Icons.close,
          color: Colors.white,
          size: 33.0 * x,
        ),
        onTap: disableSearch,
      ),
    ];

    return Container(
      height: 50.0 * x,
      color: ColorPallet.primaryColor,
      child:
          Row(children: searchEnabled ? searchDisabled : searchEnabledWidgets),
    );
  }
}

class _ExpenseListWidget extends StatelessWidget {
  List<String> annotedTransactions;
  String lastTransactionID;
  String today = DateFormat('yyyy-MM-dd').format(DateTime.now());
  String yesterday = DateFormat('yyyy-MM-dd')
      .format(DateTime.now().subtract(new Duration(days: 1)));

  Future<List<Map<String, dynamic>>> getTransactions(
      context, filtermodel) async {
    List<Map<String, dynamic>> allRows =
        await TransactionDatabase.queryAllTransactions(context);

    allRows = allRows.reversed.toList();
    int lastDate = 0;
    annotedTransactions = [];

    allRows.forEach(
      (transaction) {
        int newDate = transaction['date'];
        if (newDate != lastDate) {
          annotedTransactions.add(transaction['transactionID']);
          lastDate = newDate;
        }
      },
    );
    return allRows;
  }

  Widget getDateWidget(Map<String, dynamic> transaction) {
    String newDate = transaction['date'].toString();
    Widget dateHeader;

    if (annotedTransactions != null) {
      if (annotedTransactions.contains(transaction['transactionID'])) {
        if (newDate == today) {
          newDate = Translations.textStatic('today', 'Manual_Entry');
        } else if (newDate == yesterday) {
          newDate = Translations.textStatic('yesterday', 'Manual_Entry');
        } else {
          newDate = DateFormat('EEEE, d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
              .format(DateTime.parse(newDate));
          newDate = newDate[0].toUpperCase() + newDate.substring(1);
        }
        dateHeader = Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10 * y),
          child: Text(newDate,
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15.0 * f,
                  fontWeight: FontWeight.w500)),
        );
      } else {
        dateHeader = Container();
      }
    } else {
      dateHeader = Container();
    }
    return dateHeader;
  }

  @override
  Widget build(BuildContext context) {
    updatedContextRef = context;

    lastDate = "";
    lastTransactionID = "";
    return Container(
      child: ScopedModelDescendant<FilterModel>(
        builder: (_context2, _child2, filtermodel) =>
            ScopedModelDescendant<ExpenseListModel>(
          builder: (context, child, expensemodel) =>
              ScopedModelDescendant<NewTransactionModel>(
            builder: (_context1, _child1, transactionmodel) => FutureBuilder(
              future: getTransactions(context, filtermodel),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
                if (!snapshot.hasData || snapshot.data.length == 0) {
                  updatedContextRef = context;
                  return _NoExpenseWidget();
                }
                return ListView.builder(
                  scrollDirection: Axis.vertical,
                  padding: EdgeInsets.symmetric(
                      horizontal: 6.0 * x, vertical: 6 * y),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: <Widget>[
                        getDateWidget(snapshot.data[index]),
                        StoreItemWidget(
                            snapshot.data[index], false, false, false),
                      ],
                    );
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

class _NoExpenseWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset(
          "images/no_items_found_icon.png",
          height: 85.0 * y,
          width: 85.0 * x,
        ),
        SizedBox(height: 10.0 * y),
        Text(
          Translations.textStatic('noExpensesFound', 'Manual_Entry'),
          textAlign: TextAlign.center,
          style: TextStyle(
              color: ColorPallet.midGray,
              fontWeight: FontWeight.w700,
              fontSize: 14.0 * f),
        ),
      ],
    );
  }
}

class StoreItemWidget extends StatefulWidget {
  StoreItemWidget(this.transaction, this.spaceBeforeFirst, this.spaceAfterLast,
      this.confirmDayPage);

  final bool confirmDayPage;
  final bool spaceAfterLast;
  final bool spaceBeforeFirst;
  final Map<String, dynamic> transaction;

  @override
  _StoreItemWidgetState createState() => _StoreItemWidgetState();
}

class _StoreItemWidgetState extends State<StoreItemWidget> {
  bool modifyingTransaction = false;

  Future<List<Map<String, dynamic>>> getProducts(String transaction) async {
    List<Map<String, dynamic>> allRows =
        await TransactionDatabase.queryTransactionProducts(transaction);
    allRows = allRows.reversed.toList();
    return GroupProducts.getProductGroups(allRows);
  }

  Widget abroadWidget() {
    if (widget.transaction['expenseAbroad'] == 'true') {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 7),
        child: Row(
          children: <Widget>[
            SizedBox(width: 18 * x),
            Text(Translations.textStatic('abroad', 'Manual_Entry'),
                style: TextStyle(
                    fontSize: 14 * f,
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w700)),
            Expanded(
              child: Container(),
            ),
            Icon(Icons.check, color: ColorPallet.pink, size: 20),
            SizedBox(width: 30 * x),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  Widget onlineWidget() {
    if (widget.transaction['expenseOnline'] == 'true') {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 7),
        child: Row(
          children: <Widget>[
            SizedBox(width: 18 * x),
            Text( Translations.textStatic('online', 'Manual_Entry'),
                style: TextStyle(
                    fontSize: 14 * f,
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w700)),
            Expanded(
              child: Container(),
            ),
            Icon(Icons.check, color: ColorPallet.pink, size: 20),
            SizedBox(width: 30 * x),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  Widget discountWidget() {
    if (double.parse(widget.transaction['discountAmount']) > 0 ||
        double.parse(widget.transaction['discountPercentage']) > 0) {
      String discount;
      if (double.parse(widget.transaction['discountAmount']) > 0) {
        discount = "-" + widget.transaction['discountAmount'];
      }
      if (double.parse(widget.transaction['discountPercentage']) > 0) {
        discount = "-" + widget.transaction['discountPercentage'] + "%";
      }
      return Container(
        padding: EdgeInsets.symmetric(vertical: 7),
        child: Row(
          children: <Widget>[
            SizedBox(width: 18 * x),
            Text(Translations.textStatic('discount', 'Manual_Entry'),
                style: TextStyle(
                    fontSize: 14 * f,
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w700)),
            Expanded(
              child: Container(),
            ),
            Text(discount,
                style: TextStyle(
                    fontSize: 14 * f,
                    color: ColorPallet.pink,
                    fontWeight: FontWeight.w700)),
            SizedBox(width: 30 * x),
          ],
        ),
      );
    }
    return Container();
  }

  Icon getStoreIcon(String storeType) {
    if (IconMap.icon.containsKey(storeType)) {
      return Icon(IconMap.icon[storeType][0],
          color: ColorPallet.darkTextColor,
          size: IconMap.icon[storeType][2] ? 19 * x : 25 * x);
    }
    return Icon(Icons.store, size: 25 * x, color: ColorPallet.darkTextColor);
  }

  void showMySnackBar(String deletedTransactiondID) async {
    var snackBar = SnackBar(
      content: Text( Translations.textStatic('transactionRemoved', 'Manual_Entry')),
      action: SnackBarAction(
        textColor: ColorPallet.primaryColor,
        label: Translations.textStatic('undo', 'Manual_Entry'),
        onPressed: () async {
          await TransactionDatabase.restoreBackUp(deletedTransactiondID);
          await SyncDatabase.updateSync(
              RECEIPT_TYPE, deletedTransactiondID, UPDATED);
          NewTransactionModel.of(updatedContextRef).notifyListeners();
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    String store = widget.transaction['store'];
    String storeType = widget.transaction['storeType'];
    String totalPrice = widget.transaction['totalPrice'].toString();
    String receiptLocation = widget.transaction['receiptLocation'];

    if (double.parse(totalPrice) > 1000) {
      totalPrice = double.parse(totalPrice).toStringAsFixed(0);
    }

    updatedContextRef = context;

    return Column(children: <Widget>[
      widget.spaceBeforeFirst ? SizedBox(height: 70 * y) : Container(),
      Container(
          margin: EdgeInsets.symmetric(horizontal: 6.0 * x, vertical: 6.0 * y),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: ColorPallet.veryLightGray,
                  offset: Offset(5.0 * x, 1.0 * y),
                  blurRadius: 2.0 * x,
                  spreadRadius: 3.0 * x)
            ],
          ),
          child: Theme(
            data: ThemeData(accentColor: ColorPallet.primaryColor),
            child: ExpansionTile(
              title: Row(
                children: <Widget>[
                  Container(
                    width: 145 * x,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          store,
                          style: TextStyle(
                              fontSize: 18 * f,
                              color: ColorPallet.darkTextColor,
                              fontWeight: FontWeight.w700),
                        ),
                        Text(
                          storeType,
                          style: TextStyle(
                              fontSize: 14 * f,
                              color: ColorPallet.midGray,
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ),
                  receiptLocation == ""
                      ? Container()
                      : Icon(
                          Icons.camera_alt,
                          color: ColorPallet.darkTextColor,
                          size: 24 * x,
                        ),
                  receiptLocation == "" ? Container() : SizedBox(width: 10 * x),
                  Expanded(child: Container()),
                  Text(
                    double.parse(totalPrice).toStringAsFixed(2),
                    style: TextStyle(
                        fontSize: 16.5 * f,
                        color: ColorPallet.pink,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              ),
              leading: getStoreIcon(storeType),
              children: <Widget>[
                SizedBox(height: 5 * y),
                SizedBox(height: 5 * y),
                receiptLocation == ""
                    ? Container()
                    //: Image.asset(receiptLocation),
                    : Image.file(
                        File(receiptLocation),
                      ),
                receiptLocation != ""
                    ? Container()
                    : FutureBuilder(
                        future:
                            getProducts(widget.transaction['transactionID']),
                        builder: (BuildContext context,
                            AsyncSnapshot<List<Map<String, dynamic>>>
                                snapshot) {
                          if (!snapshot.hasData) {
                            return Container();
                          }
                          return Column(
                              children: snapshot.data.map((productInfo) {
                            return _ProductItemWidget(productInfo);
                          }).toList());
                        },
                      ),
                SizedBox(height: 5 * y),
                discountWidget(),
                onlineWidget(),
                abroadWidget(),
                Container(
                  height: 65 * y,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      InkWell(
                        onTap: () async {
                          await TransactionDatabase.deleteTransactionWithBackup(
                              widget.transaction['transactionID']);
                          await SyncDatabase.updateSync(RECEIPT_TYPE,
                              widget.transaction['transactionID'], DELETED);

                          //await TransactionDatabase.deleteTransaction(   widget.transaction['transactionID']);
                          NewTransactionModel.of(context).notifyListeners();
                          showMySnackBar(widget.transaction['transactionID']);
                        },
                        child: Container(
                          width: 115 * x,
                          height: 25 * y,
                          decoration: BoxDecoration(
                            color: ColorPallet.darkTextColor,
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.delete,
                                color: Colors.white,
                                size: 16 * x,
                              ),
                              Text(Translations.textStatic('delete', 'Manual_Entry'),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13 * f)),
                              SizedBox(width: 1 * y),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          if (!modifyingTransaction) {
                            setState(() {
                              modifyingTransaction = true;
                            });

                            await NewTransactionModel.of(context)
                                .modifyTransaction(
                                    widget.transaction['transactionID']);

                            SystemChrome.setSystemUIOverlayStyle(
                                SystemUiOverlayStyle(
                              statusBarColor: ColorPallet.pink,
                            ));

                            if (receiptLocation == "") {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NewEntryPage(),
                                ),
                              );
                            } else {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ReceiptDetailsPage(),
                                ),
                              );
                            }
                            setState(() {
                              modifyingTransaction = false;
                            });
                          }
                        },
                        child: Container(
                          width: 115 * x,
                          height: 25 * y,
                          decoration: BoxDecoration(
                            color: ColorPallet.darkTextColor,
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.mode_edit,
                                color: Colors.white,
                                size: 16 * x,
                              ),
                              Text(Translations.textStatic('edit', 'Manual_Entry'),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13 * f)),
                              SizedBox(width: 1 * y),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          await NewTransactionModel.of(context)
                              .modifyTransaction(
                                  widget.transaction['transactionID']);

                          NewTransactionModel.of(context).modifyTransactionID =
                              null;

                          if (!widget.confirmDayPage) {
                            NewTransactionModel.of(context).date =
                                DateFormat('yyyy-MM-dd').format(DateTime.now());
                          }

                          await NewTransactionModel.of(context)
                              .addNewTransAction();
                        },
                        child: Container(
                          width: 115 * x,
                          height: 25 * y,
                          decoration: BoxDecoration(
                            color: ColorPallet.darkTextColor,
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.content_copy,
                                color: Colors.white,
                                size: 16 * x,
                              ),
                              Text(Translations.textStatic('duplicate', 'Manual_Entry'),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13 * f)),
                              SizedBox(width: 1 * y),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
      widget.spaceAfterLast ? SizedBox(height: 80 * y) : Container(),
    ]);
  }
}

class _ProductItemWidget extends StatelessWidget {
  _ProductItemWidget(this.productInfo);

  String discount;
  String itemCount;
  String itemPrice;
  String product;
  String productCategory;
  final Map<String, dynamic> productInfo;
  String totalPrice;

  @override
  Widget build(BuildContext context) {
    product = productInfo['product'];
    productCategory = productInfo['productCategory'];
    totalPrice = productInfo['totalPrice'].toString();
    itemCount = productInfo['itemCount'];
    itemPrice = productInfo['itemPrice'];

    return Padding(
      padding: EdgeInsets.only(top: 8.0 * y, bottom: 8.0 * y, left: 0 * x),
      child: Row(
        children: <Widget>[
          Container(
            width: 70 * x,
            child: Column(children: <Widget>[
              Text(
                itemCount + "x",
                style: TextStyle(
                    fontSize: 14 * f,
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w600),
              ),
              Text(
                double.parse(itemPrice).toStringAsFixed(2),
                style: TextStyle(
                    fontSize: 14 * f,
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w600),
              ),
            ]),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: 225 * x,
                child: Text(
                  product,
                  style: TextStyle(
                      fontSize: 14 * f,
                      color: ColorPallet.darkTextColor,
                      fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(height: 5 * y),
              Container(
                width: 225 * x,
                child: Text(
                  productCategory,
                  style: TextStyle(
                      fontSize: 14 * f,
                      color: ColorPallet.midGray,
                      fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      double.parse(totalPrice).toStringAsFixed(2),
                      style: TextStyle(
                          fontSize: 14 * f,
                          color: ColorPallet.pink,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 5 * y),
                  ],
                ),
                SizedBox(width: 30 * x),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
