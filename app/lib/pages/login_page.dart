import 'package:barcode_scan/barcode_scan.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/util/login_setup.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:budget_onderzoek/translations/translations.dart';

Translations translations;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: ColorPallet.lightBlueWithOpacity),
    );
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Login');
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _QRLoginWidget(),
              _LoginManualWidget(),
            ],
          ),
        ),
      ),
    );
  }
}

class _QRLoginWidget extends StatefulWidget {
  @override
  __QRLoginWidgetState createState() => __QRLoginWidgetState();
}

class __QRLoginWidgetState extends State<_QRLoginWidget> {
  String barcode = "";

  void scan() async {
    try {
      ScanResult barcode = await BarcodeScanner.scan();
      setState(() {
        this.barcode = barcode.rawContent;
        Login.validateInput(context, barcode.rawContent);
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 5.5 / 10,
      color: ColorPallet.lightBlueWithOpacity,
      child: Padding(
        padding: EdgeInsets.only(left: 35.0 * x, right: 10 * x),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 68 * y,
            ),
            Text(
              translations.text('welcome') + ",",
              style: TextStyle(
                color: Colors.white,
                fontSize: 42 * f,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 5 * y),
            Container(
              width: 360 * x,
              child: Text(
                translations.text('toLogin'),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22 * f,
                  fontWeight: FontWeight.w700,
                  height: 1.1,
                ),
              ),
            ),
            Container(
              height: 150 * y,
              child: Center(
                child: Row(children: <Widget>[
                  ButtonTheme(
                    minWidth: 180.0 * x,
                    height: 37.0 * y,
                    child: RaisedButton(
                      onPressed: () {
                        scan();
                      },
                      color: ColorPallet.pink,
                      child: Text(
                        translations.text('scanQRcode'),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17 * f,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(11.0),
                      ),
                    ),
                  ),
                  SizedBox(width: 60 * x),
                  Image.asset(
                    "images/qr_code_scanning.png",
                    height: 110 * y,
                  )
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _LoginManualWidget extends StatefulWidget {
  @override
  __LoginManualWidgetState createState() => __LoginManualWidgetState();
}

class __LoginManualWidgetState extends State<_LoginManualWidget> {
  String password = "";
  bool passwordInvisible = true;
  String username = "";

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 4.5 / 10,

      /// 2,
      color: ColorPallet.primaryColor,
      child: Column(
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 40 * y,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 40 * x),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: ColorPallet.lightBlueWithOpacity,
            ),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: 12.0 * x, vertical: 2 * y),
              child: TextField(
                cursorColor: ColorPallet.darkTextColor,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 19 * f,
                    color: Colors.white),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(vertical: 7 * y),
                  border: InputBorder.none,
                  icon:
                      Icon(Icons.person, color: Colors.white.withOpacity(0.7)),
                  hintText: translations.text('user'),
                  hintStyle: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 17 * f,
                      color: Colors.white.withOpacity(0.7)),
                ),
                onChanged: (value) {
                  value = UserInput.textSanitizer(value);
                  setState(() {
                    username = value;
                  });
                },
              ),
            ),
          ),
          SizedBox(height: 18 * y),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 40 * x),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: ColorPallet.lightBlueWithOpacity,
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 2),
              child: Stack(
                children: <Widget>[
                  TextField(
                    obscureText: passwordInvisible,
                    cursorColor: ColorPallet.darkTextColor,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 19 * f,
                        color: Colors.white),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 7 * y),
                      border: InputBorder.none,
                      icon: Icon(Icons.vpn_key,
                          color: Colors.white.withOpacity(0.7)),
                      hintText: translations.text('password'),
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 17 * f,
                          color: Colors.white.withOpacity(0.7)),
                    ),
                    onChanged: (value) {
                      value = UserInput.textSanitizer(value);
                      setState(() {
                        password = value;
                      });
                    },
                  ),
                  password == ""
                      ? Container()
                      : Positioned(
                          top: 6 * y,
                          right: 10 * x,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                passwordInvisible = !passwordInvisible;
                              });
                            },
                            child: !passwordInvisible
                                ? Icon(Icons.visibility,
                                    color: Colors.white.withOpacity(0.7))
                                : Icon(
                                    Icons.visibility_off,
                                    color: Colors.white.withOpacity(0.7),
                                  ),
                          ),
                        )
                ],
              ),
            ),
          ),
          Container(
            height: 120 * y,
            child: Center(
              child: ButtonTheme(
                minWidth: 330.0 * x,
                height: 37.0 * y,
                child: RaisedButton(
                  onPressed: () {
                    Login.validateInput(
                        context, username + "###***###" + password);
                  },
                  color: ColorPallet.lightGreen,
                  child: Text(
                    translations.text('loginManually'),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17 * f,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(11.0),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
