import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:budget_onderzoek/database/datamodels/user_progress_db.dart';
import 'package:budget_onderzoek/pages/main_expenselist.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/scoped_models/expenselist_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/fliter_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';

import 'camera.dart';
import 'new_entry.dart';

Translations translations;

class ConfirmDayPage extends StatefulWidget {
  ConfirmDayPage(this.date);

  final Map<dynamic, dynamic> date;

  @override
  _ConfirmDayPageState createState() => _ConfirmDayPageState();
}

class _ConfirmDayPageState extends State<ConfirmDayPage>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _colorTween;
  bool _showAnimation = false;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _colorTween =
        ColorTween(begin: ColorPallet.primaryColor, end: ColorPallet.lightGreen)
            .animate(_animationController);

    super.initState();
  }

  void setupDateNewEntry() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));
    NewTransactionModel.of(context).reset();
    DateTime picked =
        DateTime(widget.date['year'], widget.date['month'], widget.date['day']);
    NewTransactionModel.of(context).date =
        DateFormat('yyyy-MM-dd').format(picked);
    NewTransactionModel.of(context).notifyListeners();
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Confirm_Day');
    updatedContextRef = context;
    String dateString = UserProgressDatabase.getDateString(
        widget.date['year'].toString(),
        widget.date['month'].toString(),
        widget.date['day'].toString());
    return Scaffold(
      appBar: AppBar(
        title: Text(
          translations.text('expensesPage') +
              " - " +
              DateFormat('d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
                  .format(DateTime.parse(dateString)),
          style: TextStyle(color: Colors.white, fontSize: 23 * f),
        ),
      ),
      body: ScopedModelDescendant<FilterModel>(
        builder: (_context2, _child2, filtermodel) =>
            ScopedModelDescendant<ExpenseListModel>(
          builder: (context, child, expensemodel) =>
              ScopedModelDescendant<NewTransactionModel>(
            builder: (_context1, _child1, transactionmodel) => Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: FutureBuilder(
                        future: TransactionDatabase.queryDailyTransactions(
                          context,
                          widget.date['year'].toString(),
                          widget.date['month'].toString(),
                          widget.date['day'].toString(),
                        ),
                        builder: (context, snapshot) {
                          if (snapshot.data == null) {
                            return Container();
                          }
                          Widget noExpenses = Container(
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  "images/no_items_found_icon.png",
                                  height: 85.0 * y,
                                  width: 85.0 * x,
                                ),
                                SizedBox(height: 10.0 * y),
                                Text(
                                  translations.text('noExpensesFound'),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: ColorPallet.midGray,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 14.0 * f),
                                ),
                              ],
                            ),
                          );

                          return Stack(
                            children: <Widget>[
                              _showAnimation
                                  ? CheckMarkAnimation()
                                  : snapshot.data.length == 0
                                      ? noExpenses
                                      : ExpenseList(snapshot),
                              Positioned(
                                top: 15 * y,
                                child: _showAnimation == true
                                    ? Container()
                                    : Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            Container(
                                              padding:
                                                  EdgeInsets.only(left: 8 * x),
                                              width: 200 * x,
                                              child: RaisedButton.icon(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0),
                                                ),
                                                color: ColorPallet.primaryColor,
                                                icon: Icon(Icons.camera_alt,
                                                    color: Colors.white,
                                                    size: 20 * x),
                                                label: Text(
                                                  translations.text('add'),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 18 * f,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                                onPressed: () {
                                                  setupDateNewEntry();
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          CameraPage(false),
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: 8 * x, right: 8 * x),
                                              width: 200 * x,
                                              child: RaisedButton.icon(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0),
                                                ),
                                                color: ColorPallet.primaryColor,
                                                icon: Icon(Icons.keyboard,
                                                    color: Colors.white,
                                                    size: 20 * x),
                                                label: Text(
                                                  translations.text('add'),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 18 * f,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                                onPressed: () {
                                                  setupDateNewEntry();
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          NewEntryPage(),
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                              ),
                              Positioned(
                                bottom: 20 * y,
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      FutureBuilder(
                                        future:
                                            UserProgressDatabase.canCompleteDay(
                                          dateString,
                                          widget.date['year'].toString(),
                                          widget.date['month'].toString(),
                                          widget.date['day'].toString(),
                                        ),
                                        builder: (context, snapshot) {
                                          if (snapshot.data == null) {
                                            return Container();
                                          } else {
                                            print(snapshot.data);
                                            bool canComplete =
                                                snapshot.data == "canComplete";
                                            return Container(
                                              width: 380 * x,
                                              child: AnimatedBuilder(
                                                animation: _colorTween,
                                                builder: (context, child) =>
                                                    RaisedButton.icon(
                                                  onPressed: () async {
                                                    if (_showAnimation) {
                                                      Navigator.pop(context);
                                                      return true;
                                                    }
                                                    if (canComplete) {
                                                      await UserProgressDatabase
                                                          .markDayComplete(
                                                        widget.date['year']
                                                            .toString(),
                                                        widget.date['month']
                                                            .toString(),
                                                        widget.date['day']
                                                            .toString(),
                                                      );

                                                      setState(() {
                                                        _showAnimation = true;
                                                      });

                                                      if (_animationController
                                                              .status ==
                                                          AnimationStatus
                                                              .completed) {
                                                        _animationController
                                                            .reverse();
                                                      } else {
                                                        _animationController
                                                            .forward();
                                                      }

                                                      // Navigator.pop(context);
                                                    } else {
                                                      if (snapshot.data ==
                                                          "alreadyComplete") {
                                                        Toast.show(
                                                            translations.text('dayAlreadyCompleted'),
                                                            context,
                                                            duration: Toast
                                                                .LENGTH_LONG,
                                                            gravity:
                                                                Toast.CENTER);
                                                      } else {
                                                        Toast.show(
                                                            translations.text('youCanNotCompleteDays'),
                                                            context,
                                                            duration: Toast
                                                                .LENGTH_LONG,
                                                            gravity:
                                                                Toast.CENTER);
                                                      }
                                                    }
                                                  },
                                                  icon: Icon(
                                                    Icons.check,
                                                    color: _showAnimation
                                                        ? Colors.transparent
                                                        : Colors.white,
                                                    size: 20 * x,
                                                  ),
                                                  label: Text(
                                                    _showAnimation
                                                        ? translations.text('ok')
                                                        : snapshot.data ==
                                                                "alreadyComplete"
                                                            ? translations.text('dayAlreadyCompleted')
                                                            : translations.text('completeDay'),
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 18 * f,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  color: canComplete
                                                      ? _colorTween.value
                                                      : snapshot.data ==
                                                              "alreadyComplete"
                                                          ? ColorPallet
                                                              .lightGreen
                                                          : ColorPallet
                                                              .lightGray,
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                  ),
                                                ),
                                              ),
                                            );
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          );
                        }),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CheckMarkAnimation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 520 * y,
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 270 * y,
            width: 270 * x,
            child: FlareActor(
              "images/CheckMark.flr",
              alignment: Alignment.center,
              fit: BoxFit.contain,
              animation: "Checkmark Appear",
            ),
          ),
          SizedBox(height: 20 * y),
          Text(
            translations.text('dayCompleted'),
            style: TextStyle(
              color: ColorPallet.lightGreen,
              fontSize: 30 * f,
              fontWeight: FontWeight.w800,
            ),
          ),
          SizedBox(height: 60 * y)
        ],
      ),
    );
  }
}

class ExpenseList extends StatelessWidget {
  ExpenseList(this.snapshot);

  final AsyncSnapshot<dynamic> snapshot;

  @override
  Widget build(BuildContext context) {
    updatedContextRef = context;

    return Container(
      height: MediaQuery.of(context).size.height,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 4.0 * x),
        child: Column(
          children: <Widget>[
            SizedBox(height: 5 * y),
            Expanded(
              child: ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return StoreItemWidget(snapshot.data[index], index == 0,
                      index == snapshot.data.length - 1, true);
                },
              ),
            ),           
          ],
        ),
      ),
    );
  }
}

