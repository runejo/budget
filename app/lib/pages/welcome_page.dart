import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/util/login_setup.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:budget_onderzoek/database/datamodels/sync.dart';
import 'login_page.dart';
import 'dart:ui' as ui;
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';

Translations translations;

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  String dropdownValue;

  @override
  void initState() {
    super.initState();
    initDatabase();
    initializeTranslations();

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
          statusBarColor: ColorPallet.primaryColor,
          systemNavigationBarColor: ColorPallet.primaryColor),
    );
    Country country =
        International.countryFromCode(ui.window.locale.languageCode);
    dropdownValue = country.name;
    LanguageSetting.of(context).language = country.languageId;
    LanguageSetting.tablePreference = country.id;
  }

  _launchURL() async {
    String url = translations.text('url');
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void changeCountry(String country) {
    setState(() {
      dropdownValue = country;
    });
  }

  void initDatabase() async {
    await DatabaseHelper.instance.database;
  }

  Widget _countryIcon(String country) {
    String png = International.countryFromName(country).flagImage;
    if (png == "") {
      return Row(
        children: <Widget>[
          SizedBox(width: 30 * x, height: 110 * y),
          Text(
            country,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18 * f,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      );
    }
    return Row(
      children: <Widget>[
        SizedBox(width: 10 * x, height: 110 * y),
        Container(
            width: 26 * x,
            height: 26 * x,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    fit: BoxFit.fill, image: ExactAssetImage("images/$png")))),
        SizedBox(
          width: 10 * x,
        ),
        Text(
          country,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18 * f,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Login');
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: ColorPallet.primaryColor,
        child: Column(
          children: <Widget>[
            SizedBox(height: 130 * y),
            Container(
              height: 150 * y,
              child: Center(
                child: Text(
                  translations.text('hbsLong2'),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 50 * f,
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            SizedBox(height: 120 * y),
            Container(
              width: 310 * x,
              color: ColorPallet.primaryColor,
              child: Theme(
                data: Theme.of(context).copyWith(
                  canvasColor: ColorPallet.primaryColor,
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    iconEnabledColor: Colors.white.withOpacity(0.8),
                    value: dropdownValue,
                    elevation: 0,
                    onChanged: (String newValue) {
                      setState(() {
                        //dropdownValue = newValue;
                        changeCountry(newValue);
                        LanguageSetting.of(context).language =
                            International.countryFromName(newValue).languageId;
                        LanguageSetting.tablePreference =
                            International.countryFromName(newValue).id;
                      });
                    },
                    items: International.countries()
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                          value: value,
                          child: Container(
                            height: 45 * y,
                            decoration: new BoxDecoration(
                                color: ColorPallet.lightBlueWithOpacity,
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(10.0),
                                    topRight: const Radius.circular(10.0),
                                    bottomLeft: const Radius.circular(10.0),
                                    bottomRight: const Radius.circular(10.0))),
                            //color: ColorPallet.lightBlueWithOpacity,
                            width: 260 * x,
                            margin: const EdgeInsets.all(5.0),
                            child: _countryIcon(value),
                          ));
                    }).toList(),
                  ),
                ),
              ),
            ),
            SizedBox(height: 67 * y),
            ButtonTheme(
              minWidth: 300.0 * x,
              height: 40.0 * y,
              child: RaisedButton(
                onPressed: () {
                  if (dropdownValue != 'Please choose a country') {
                    if (true || Synchronise.backendIsActive()) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ),
                      );
                    } else {
                      Login.validateInput(context, "hbs###***###2019");
                    }
                  }
                },
                color: dropdownValue != 'Please choose a country'
                    ? ColorPallet.lightGreen
                    : ColorPallet.lightGray,
                child: Text(
                  translations.text('start'),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18 * f,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(11.0),
                ),
              ),
            ),
            Expanded(child: Container()),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text: translations.text('url'),
                  style: TextStyle(
                      color: Colors.white.withOpacity(0.8),
                      fontSize: 17.0 * f,
                      decoration: TextDecoration.underline),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      _launchURL();
                    },
                ),
                TextSpan(text: "\n"),
              ]),
            ),
            SizedBox(height: 20 * y),
          ],
        ),
      ),
    );
  }
}
