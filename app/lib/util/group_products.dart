class GroupProducts {
  static List<Map<String, String>> getProductGroups(
      List<Map<String, dynamic>> productItems) {
    List<Map<String, String>> productGroupList = [];
    String _productGroupId = '';
    Map<String, String> _productGroup = new Map();
    productItems.forEach((product) {
      if (_productGroupId != product['productGroupID']) {
        if (_productGroupId != '') {
          productGroupList.add(_productGroup);
        }
        _productGroupId = product['productGroupID'];
        _productGroup = new Map();
        _productGroup['product'] = product['product'];
        _productGroup['productCategory'] = product['productCategory'];
        _productGroup['itemPrice'] = product['price'].toString();
        _productGroup['totalPrice'] = product['price'].toString();
        _productGroup['productGroupID'] = product['productGroupID'].toString();
        _productGroup['itemCount'] = "1";
      } else {
        _productGroup['totalPrice'] = (double.parse(product['price'].toString()) +
                double.parse(_productGroup['totalPrice']))
            .toString();
        _productGroup['itemCount'] =
            (int.parse(_productGroup['itemCount']) + 1).toString();
      }
    });
    if (_productGroupId != '') {
      productGroupList.add(_productGroup);
    }
    return productGroupList;
  }
}
