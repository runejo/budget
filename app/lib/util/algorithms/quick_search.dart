import 'dart:async';
import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:sqflite/sqflite.dart';

class QuickSearch {
  static List<Map<String, dynamic>> tblQuickSearch;

  static Future<void> loadQuickSearchTable() async {
    Database db = await DatabaseHelper.instance.database;
    if (tblQuickSearch == null) {
      tblQuickSearch = await db.query(
        "tblQuickSearch" + "_" + LanguageSetting.tablePreference,
      );
    }
  }

  static Future<List<String>> getShopSuggestions(bool withSpaced) async {
    await loadQuickSearchTable();
    List<String> results = [];
    for (String key in tblQuickSearch[0].keys) {
      if (withSpaced) {
        results.add(
          key.replaceAll("_", " "),
        );
      } else {
        results.add(key);
      }
    }

    return results;
  }

  static Future<List<String>> getProductSuggestions(String shopType) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> rawResults;

    List<String> shopNames = await getShopSuggestions(false);
    String columnName = shopType.replaceAll(" ", "_");
    List<String> results = [];

    if (shopNames.contains(columnName)) {
      rawResults = await db
          .query("tblQuickSearch" + "_" + LanguageSetting.tablePreference, columns: [columnName]);
      for (Map<String, dynamic> row in rawResults) {
        if (row[columnName] != null &&
            row[columnName] != '' &&
            row[columnName] != 'NULL') {
          results.add(row[columnName]);
        }
      }
    } else {
      String altSuggestions =
          LanguageSetting.key == "nl" ? "Supermarkt" : "Grocery_store";
      rawResults = await db.query("tblQuickSearch" + "_" + LanguageSetting.tablePreference,
          columns: [altSuggestions]);
      for (Map<String, dynamic> row in rawResults) {
        if (row[altSuggestions] != null &&
            row[columnName] != '' &&
            row[columnName] != 'NULL') {
          results.add(row[altSuggestions]);
        }
      }
    }
    return results;
  }
}
