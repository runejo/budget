import 'package:flutter/widgets.dart';
import 'dart:io';

double x, y, f;

void initializeUIParemeters(BuildContext context) {
  x = MediaQuery.of(context).size.width / 411.42857142857144;
  y = MediaQuery.of(context).size.height / 683.4285714285714;
  f = (x + y) / 2;

  if (Platform.isIOS) {
    f = f * 0.9;
  }
}
