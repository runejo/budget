import 'dart:async';
import 'package:budget_onderzoek/pages/main_expenselist.dart';
import 'package:budget_onderzoek/pages/main_insights.dart';
import 'package:budget_onderzoek/pages/main_overview.dart';
import 'package:budget_onderzoek/pages/main_settings.dart';
import 'package:budget_onderzoek/pages/camera.dart';
import 'package:budget_onderzoek/pages/new_entry.dart';
import 'package:budget_onderzoek/pages/welcome_page.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/scoped_models/expenselist_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/fliter_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/settings_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:budget_onderzoek/widgets/custom_flutter/bottom_navigation_bar.dart'
    as customBottomNavigationBar;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math' as math;

import 'database/datamodels/user_progress_db.dart';
import 'util/responsive_ui.dart';

bool initialized;

GlobalKey keyButton4 = GlobalKey();
Translations translations;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  LanguageSetting.key = prefs.getString('languagePreference') ?? "en";
  LanguageSetting.tablePreference = prefs.getString('tablePreference') ?? "en";
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
  );
  initialized = await UserProgressDatabase.isInitialized();
  await initializeTranslations();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<LanguageSetting>(
      model: LanguageSetting(),
      child: ScopedModelDescendant<LanguageSetting>(
        builder: (_context, _child, _model) => ScopedModel<SettingsModel>(
          model: SettingsModel(),
          child: ScopedModel<FilterModel>(
            model: FilterModel(),
            child: ScopedModel<ExpenseListModel>(
              model: ExpenseListModel(),
              child: ScopedModel<NewTransactionModel>(
                model: NewTransactionModel(),
                child: MaterialApp(
                  builder: (BuildContext context, Widget child) {
                    final MediaQueryData data = MediaQuery.of(context);
                    return MediaQuery(
                      data: data.copyWith(textScaleFactor: 1),
                      child: child,
                    );
                  },
                  debugShowCheckedModeBanner: false,
                  localizationsDelegates: [
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                  ],
                  supportedLocales: International.locales(),
                  theme: ThemeData(
                    primarySwatch: const MaterialColor(
                      0xFF00A1CD,
                      {
                        50: Color(0xFF00A1CD),
                        100: Color(0xFF00A1CD),
                        200: Color(0xFF00A1CD),
                        300: Color(0xFF00A1CD),
                        400: Color(0xFF00A1CD),
                        500: Color(0xFF00A1CD),
                        600: Color(0xFF00A1CD),
                        700: Color(0xFF00A1CD),
                        800: Color(0xFF00A1CD),
                        900: Color(0xFF00A1CD),
                      },
                    ),
                    //fontFamily: 'Source Sans Pro'
                  ),
                  home: _AppState(),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _AppState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
        translations = Translations(context, 'Calendar');

    initializeUIParemeters(context);
    if (initialized) {
      return PageNavigator();
    } else {
      return WelcomePage();
    }
  }
}

class PageNavigator extends StatefulWidget {
  @override
  _PageNavigatorState createState() => _PageNavigatorState();
}

class _PageNavigatorState extends State<PageNavigator>
    with TickerProviderStateMixin {
  static const List<IconData> icons = const [
    Icons.camera_alt,
    Icons.keyboard,
  ];

  AnimationController _controller;
  int _currentPageIndex = 0;
  final List<Widget> _pages = [
    OverviewPage(keyButton4),
    TransactionsPage(),
    Container(),
    InsightsPage(),
    SettingsPage(),
  ];

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 250),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Container(
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      color: ColorPallet.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              bottom: 10 * y,
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      key: keyButton4,
                      height: 50.0 * y,
                      width: 56.0 * x,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              child: SafeArea(
                child: Scaffold(
                  floatingActionButtonLocation:
                      FloatingActionButtonLocation.centerDocked,
                  floatingActionButton: Container(
                    height: 210.0 * y,
                    width: 63.0 * x,
                    // margin: EdgeInsets.only(top: 60.0 * y),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.min,
                      children: List.generate(icons.length, (int index) {
                        Widget child = Container(
                          height: 70.0 * y,
                          width: 56.0 * x,
                          alignment: FractionalOffset.topCenter,
                          child: ScaleTransition(
                            scale: CurvedAnimation(
                              parent: _controller,
                              curve: Interval(
                                  0.0, 1.0 - index / icons.length / 2.0,
                                  curve: Curves.easeOut),
                            ),
                            child: FloatingActionButton(
                              heroTag: null,
                              backgroundColor: ColorPallet.pink,
                              child: Icon(icons[index],
                                  color: Colors.white, size: 26 * x),
                              onPressed: () {
                                _controller.reverse();
                                showFirstExpenseListTutorial = false;
                                _currentPageIndex = 1;
                                NewTransactionModel.of(context).reset();
                                if (icons[index] == Icons.camera_alt) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => CameraPage(false),
                                    ),
                                  );
                                } else if (icons[index] == Icons.keyboard) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => NewEntryPage(),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                        );
                        return child;
                      }).toList()
                        ..add(
                          Container(
                            width: 69.0 * y,
                            height: 69.0 * y,
                            child: FittedBox(
                              child: FloatingActionButton(
                                heroTag: null,
                                child: AnimatedBuilder(
                                  animation: _controller,
                                  builder:
                                      (BuildContext context, Widget child) {
                                    return Transform(
                                      transform: Matrix4.rotationZ(
                                          _controller.value * 0.75 * math.pi),
                                      alignment: FractionalOffset.center,
                                      child: Icon(
                                        _controller.isDismissed
                                            ? Icons.add
                                            : Icons.add,
                                      ),
                                    );
                                  },
                                ),
                                onPressed: () {
                                  if (_controller.isDismissed) {
                                    _controller.forward();
                                  } else {
                                    _controller.reverse();
                                  }
                                },
                              ),
                            ),
                          ),
                        ),
                    ),
                  ),
                  body: Container(
                    color: ColorPallet.primaryColor,
                    child: SafeArea(
                      child: Container(
                          child: _pages[_currentPageIndex],
                          color: Colors.white),
                    ),
                  ),
                  bottomNavigationBar: ScopedModelDescendant<SettingsModel>(
                      builder: (context, child, model) {
                    return Theme(
                      data: Theme.of(context).copyWith(
                          canvasColor: Colors.white,
                          primaryColor: Colors.blue,
                          textTheme: Theme.of(context).textTheme.copyWith(
                              caption: new TextStyle(color: Colors.grey))),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: ColorPallet.veryLightGray,
                                offset: Offset(0.0 * x, -2.2 * y),
                                blurRadius: 1.7 * y,
                                spreadRadius: 1.7 * y)
                          ],
                        ),
                        child: customBottomNavigationBar.BottomNavigationBar(
                          elevation: 0,
                          iconSize: 30.0 * x,
                          onTap: (index) {
                            setState(() {
                              if (index != 2) {
                                showFirstExpenseListTutorial = true;
                                ExpenseListModel.of(context).reset();
                                _currentPageIndex = index;
                              }

                              _controller.reverse();
                            });
                          },
                          fixedColor: ColorPallet.primaryColor,
                          type: customBottomNavigationBar
                              .BottomNavigationBarType.fixed,
                          currentIndex: _currentPageIndex,
                          items: [
                            {
                              "icon": Icons.event_note,
                              "text":translations.text('overviewPage')
                            },
                            {
                              "icon": Icons.receipt,
                              "text": translations.text('expensesPage')
                            },
                            {"icon": Icons.add, "text": ''},
                            {
                              "icon": Icons.equalizer,
                              "text": translations.text('insightsPage')
                            },
                            {
                              "icon": Icons.settings,
                              "text": translations.text('settings')
                            },
                          ].map(
                            (Map<String, dynamic> values) {
                              return BottomNavigationBarItem(
                                icon: Icon(values['icon']),
                                title: Text(
                                  values['text'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 13.0 * f),
                                ),
                              );
                            },
                          ).toList(),
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
