import 'package:flutter/material.dart';

class Language {
  String id;
  String name;
  String languageCode;
  String countryCode;

  Language(this.id, this.name, this.languageCode, this.countryCode);

  Locale get locale {
    if (countryCode.isEmpty) {
      return Locale(languageCode);
    } else {
      return Locale(languageCode, countryCode);
    }
  }

  String get localeKey {
    if (countryCode.isEmpty) {
      return languageCode;
    } else {
      return languageCode + '_' + countryCode;
    }
  }
}

class Country {
  String id;
  String name;
  String languageId;
  String flagImage;
  String topbarImage;

  Country(
      this.id, this.name, this.languageId, this.flagImage, this.topbarImage);
}

class International {
  static List<Language> _languages = [
    Language('be_nl', 'Belgisch Nederlands', 'nl', 'BE'),
    Language('de', 'Deutsch', 'de', ''),
    Language('en', 'English', 'en', 'US'),
    Language('es', 'Español', 'es', ''),
    Language('be_fr', 'Français Belge', 'fr', 'BE'),
    Language('lu', 'Lëtzebuerger', 'de', 'LU'),
    Language('nl', 'Nederlands', 'nl', ''),
    Language('no', 'Norsk', 'nb', 'NO'),
    Language('pl', 'Polsku', 'pl', ''),
    Language('sl', 'Slovensko', 'sl', ''),
    Language('fi', 'Suomi', 'fi', '')
  ];

  static List<Country> _countries = [
    Country('de', 'Deutschland', 'de', 'GERM0001.png', 'topbar_de.png'),
    Country('es', 'España', 'es', 'Spain.png', 'topbar_es.png'),
    Country('nl', 'Nederland', 'nl', 'Netherlands.png', 'topbar.png'),
    Country('no', 'Norge', 'no', 'Norway.png', 'topbar_no.png'),
    Country('sl', 'Slovenija', 'sl', 'Slovenia.png', 'topbar_sl.png'),
    Country('fi', 'Suomi', 'fi', 'Finland.png', 'topbar_fin.png'),
    Country('en', 'United Kingdom', 'en', 'britain.png', 'top_empty.png'),
    Country('be_nl', 'Belgie', 'be_nl', 'top_empty.png', 'top_empty.png'),
    Country('be_fr', 'Belgie', 'be_fr', 'top_empty.png', 'top_empty.png'),
    Country('lu', 'Lëtzebuerger', 'lu', 'top_empty.png', 'top_empty.png'),
    Country('pl', 'Polsku', 'en', 'top_empty.png', 'top_empty.png'),
  ];

  static Map<String, String> _countryCodes = {
    'de': 'de',
    'es': 'es',
    'nl': 'nl',
    'no': 'no',
    'nb': 'no',
    'nn': 'no',
    'sk': 'sl',
    'sl': 'sl',
    'fi': 'fi',
    'en': 'en'
  };

  //### get item ############################################################

  static Language languageFromId(String id) {
    return _languages.firstWhere((x) => x.id == id);
  }

  static Language languageFromName(String name) {
    return _languages.firstWhere((x) => x.name == name);
  }

  static Country countryFromId(String id) {
    return _countries.firstWhere((x) => x.id == id);
  }

  static Country countryFromName(String name) {
    return _countries.firstWhere((x) => x.name == name);
  }

  static Country countryFromCode(String code) {
    if (_countryCodes.keys.contains(code)) {
      return countryFromId(_countryCodes[code]);
    } else {
      return countryFromId(_countryCodes['en']);
    }
  }

  //### get list ############################################################

  static List<String> languages() {
    List<String> result = List<String>();
    for (Language l in _languages) {
      result.add(l.name);
    }
    return result;
  }

  static List<String> countries() {
    List<String> result = List<String>();
    for (Country c in _countries) {
      result.add(c.name);
    }
    return result;
  }

  static List<Locale> locales() {
    List<Locale> result = List<Locale>();
    for (Language l in _languages) {
      result.add(l.locale);
    }
    return result;
  }
}
