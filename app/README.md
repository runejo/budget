# Budget App

A budget app built at Statistics Netherlands (Centraal Bureau voor de Statistiek). 

This app is built with Flutter.

## Screenshots
<img src="screenshots/login.png"  width="196" height="350">
<img src="screenshots/overview.png"  width="196" height="350">
<img src="screenshots/adding_manually.png"  width="196" height="350">
<img src="screenshots/expense_list.png"  width="196" height="350">
<img src="screenshots/insights_category_view.png"  width="196" height="350">

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
