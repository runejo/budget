package main

import (
	"budget/global"
	"budget/database"
	"budget/restapi"
	"log"
	"net/http"
	"strconv"
	"github.com/gorilla/mux"
)

//Read the README in budget/passwords/generate_passwords.go
//STAGE : 0 = create JackJanssen, 1 = create users, 2 = run budget 
const STAGE int = 0

const STOPONMISSINGENVIRONMENT bool = false

func main() {
	var port 		string = global.GetEnvironment("PORT"			, STOPONMISSINGENVIRONMENT)
	var host 		string = global.GetEnvironment("BUDGET_HOST"	, STOPONMISSINGENVIRONMENT)
	var dbname 		string = global.GetEnvironment("BUDGET_DBNAME"	, STOPONMISSINGENVIRONMENT)
	var dbport 		string = global.GetEnvironment("BUDGET_PORT"	, STOPONMISSINGENVIRONMENT)
	var user 		string = global.GetEnvironment("BUDGET_USER"	, STOPONMISSINGENVIRONMENT)
	var password 	string = global.GetEnvironment("BUDGET_PASSWORD", STOPONMISSINGENVIRONMENT)

	var dbportInt int
	dbportInt, _ = strconv.Atoi(dbport)

	database.Init(host, dbportInt, user, password, dbname)

	if STAGE == 0 {
		database.CreateSuperUsers()
	} else if STAGE == 1 {
		database.CreateUsers()
	}

	router := mux.NewRouter()
	
	/*
	router.HandleFunc("/budget/{entry}"											, restapi.TestApp).Methods("GET")
	router.HandleFunc("/budget/user/{userName}/{password}/{users}/{action}"		, restapi.CreateUsers).Methods("GET")
	router.HandleFunc("/budget/user/{userName}/{password}"						, restapi.RegisterUser).Methods("POST")
	router.HandleFunc("/budget/phone/{userGuid}/{phoneName}"					, restapi.RegisterPhone).Methods("POST")
	router.HandleFunc("/budget/phone/{userGuid}/{phoneGuid}"					, restapi.GetLastPhoneUpdate).Methods("GET")
	router.HandleFunc("/budget/data/{userGuid}/{phoneGuid}/{datatype}/{update}"	, restapi.PullData).Methods("GET")
	router.HandleFunc("/budget/data/{userGuid}/{phoneGuid}/{datatype}/{update}"	, restapi.PushData).Methods("POST")
*/

/*
	router.HandleFunc("/budget/{entry}"											, restapi.TestApp).Methods("GET")
	router.HandleFunc("/budget/user/{userName}/{password}/{users}/{action}"		, restapi.CreateUsers).Methods("GET")
	router.HandleFunc("/budget/user/{userName}/{password}"				    vervalt		, restapi.RegisterUser).Methods("POST")

	router.HandleFunc("/budget/phone/{username}/{password}/{phonename}"			, restapi.RegisterPhone2).Methods("POST")
	router.HandleFunc("/budget/phone/{username}/{password}/{phonename}"		vervalt	, restapi.GetLastPhoneUpdate).Methods("GET")

	router.HandleFunc("/budget/data/{username}/{password}/{phonename}/{datatype}/{update}"	, restapi.PullData2).Methods("GET")
	router.HandleFunc("/budget/data/{username}/{password}/{phonename}/{datatype}/{update}"	, restapi.PushData2).Methods("POST")
*/


router.HandleFunc("/budget/{entry}"														, restapi.TestApp).Methods("GET")
router.HandleFunc("/budget/user/{userName}/{password}/{users}/{action}"					, restapi.CreateUsers).Methods("GET")
router.HandleFunc("/budget/phone/{username}/{password}/{phonename}"						, restapi.RegisterPhone2).Methods("POST")
router.HandleFunc("/budget/data/{username}/{password}/{phonename}/{datatype}/{update}"	, restapi.PullData2).Methods("GET")
router.HandleFunc("/budget/data/{username}/{password}/{phonename}/{datatype}/{update}"	, restapi.PushData2).Methods("POST")

	log.Fatal(http.ListenAndServe(":"+port, router))
}

