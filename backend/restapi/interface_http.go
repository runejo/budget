package restapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"

	"budget/database"
	"budget/global"
	"budget/passwords"
)

var err error

func TestApp(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("TestApp")
	params := mux.Vars(r)
	var goAppTest global.GoAppTest

	goAppTest.Entry = params["entry"]
	goAppTest.Reply = "GO Budget web-service is running."

	json.NewEncoder(w).Encode(goAppTest)
}

func RegisterUser(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("RegisterUser")

	params := mux.Vars(r)

	var user global.User
	user, err = database.RegisterUser(params["userName"], params["password"])

	json.NewEncoder(w).Encode(user)
}

func CreateUsers(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("CreateUsers")

	params := mux.Vars(r)
	var users []global.UserPassword

	var user global.User
	user, err = database.RegisterUser(params["userName"], params["password"])
	if err != nil {
		global.MyPrint(err.Error())
		users = append(users, global.UserPassword{Name: params["userName"], Password: "Invalid user/password!"})
	} else if user.User_Type != 1 {
		global.MyPrint(err.Error())
		users = append(users, global.UserPassword{Name: params["userName"], Password: "Only superusers alowed!"})
	} else {
		set := strings.Split(params["users"], ",")
		act := params["action"]

		if act == "superuser" {
			if len(set) == 1 {
				user, _ := database.UpdateUserType(params["users"], 1)
				users = append(users, global.UserPassword{Name: user.Name, Password: "= SuperUser"})
			}
		} else if act == "user" {
			if len(set) == 1 {
				user, _ := database.UpdateUserType(params["users"], 0)
				users = append(users, global.UserPassword{Name: user.Name, Password: "= NormalUser"})
			}
		} else if act == "set" {
			if len(set) == 2 {
				user, err = database.CreateUserPassword(set[0], set[1], 0, true)
				user.Password = set[1]
				users = append(users, global.UserPassword{Name: user.Name, Password: user.Password})
			}
		} else if act == "create" {
			if len(set) == 1 {
				pwd := passwords.CreatePassword("ULLD__MULD")
				user, err = database.CreateUserPassword(params["users"], pwd, 0, true)
				user.Password = pwd
				users = append(users, global.UserPassword{Name: user.Name, Password: user.Password})
			} else if len(set) == 2 {
				nnn, err := strconv.Atoi(set[1])
				if err == nil {
					for n := 0; n < nnn; n++ {
						usr := set[0] + strconv.Itoa(1000 + n)[1:4]
						pwd := passwords.CreatePassword("ULLD__MULD")
						user, err = database.CreateUserPassword(usr, pwd, 0, true)
						user.Password = pwd
						users = append(users, global.UserPassword{Name: user.Name, Password: user.Password})
					}
				}
			}
		} else if act == "list" {
			if len(set) == 1 {
				user, err = database.GetUserInfo(params["users"])
				if err != nil {
					users = append(users, global.UserPassword{Name: params["users"], Password: "DOES NOT EXIST!"})
				} else {
					users = append(users, global.UserPassword{Name: params["users"], Password: "CLASSIFIED"})
				}
			} else if len(set) == 2 {
				nnn, err := strconv.Atoi(set[1])
				if err == nil {
					for n := 0; n < nnn; n++ {
						usr := set[0] + strconv.Itoa(1000 + n)[1:4]
						user, err = database.GetUserInfo(usr)
						if err != nil {
							users = append(users, global.UserPassword{Name: usr, Password: "DOES NOT EXIST!"})
						} else {
							users = append(users, global.UserPassword{Name: usr, Password: "CLASSIFIED"})
						}
					}
				}
			}
		}

	}

	json.NewEncoder(w).Encode(users)
}

func RegisterPhone(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("RegisterPhone")

	params := mux.Vars(r)
	var userGuid, _ = uuid.FromString(params["userGuid"])

	var phone global.Phone
	phone, err = database.RegisterPhone(userGuid, params["phoneName"])

	json.NewEncoder(w).Encode(phone)
}


//router.HandleFunc("/budget/phone/{username}/{password}/{phonename}"						, restapi.RegisterPhone).Methods("POST")

func RegisterPhone2(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("RegisterPhone")

	params := mux.Vars(r)

	var phone global.Phone
	phone, err = database.RegisterPhone2(params["username"], params["password"], params["phonename"])

	json.NewEncoder(w).Encode(phone)
}

func GetLastPhoneUpdate(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("GetLastPhoneUpdate")

	params := mux.Vars(r)
	var userGuid, _ = uuid.FromString(params["userGuid"])
	var phoneGuid, _ = uuid.FromString(params["phoneGuid"])

	var phone global.Phone
	phone, err = database.GetPhone(userGuid, phoneGuid)

	json.NewEncoder(w).Encode(phone)
}

func PullData(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var user global.User
	var phone global.Phone
	var update int64
	var data_type int

	global.MyPrint("#")
	global.MyPrint("PullData: " + params["datatype"])

	var userGuid, _ = uuid.FromString(params["userGuid"])
	var phoneGuid, _ = uuid.FromString(params["phoneGuid"])

	user, err = database.GetUser(userGuid)
	if err != nil {
		badRequest(w, true, "userGuid does not exist!")
		return
	}
	phone, err = database.GetPhone(userGuid, phoneGuid)
	if err != nil {
		badRequest(w, true, "phoneGuid does not exist!")
		return
	}
	update, err = strconv.ParseInt(params["update"], 10, 64)
	if err != nil {
		badRequest(w, true, "update does not exist, or is not a number!")
		return
	}
	data_type, err = global.DataType(params["datatype"])
	if err != nil {
		badRequest(w, true, "datatype does not exist!")
		return
	}

	if data_type == global.RECEIPT_TYPE {
		var receiptData global.ReceiptData
		receiptData, _ = database.PullNextReceiptData(user.Id, phone.Id, update)
		json.NewEncoder(w).Encode(receiptData)
	} else if data_type == global.PRODUCT_TYPE {
		var searchProductData global.SearchProductData
		searchProductData, _ = database.PullNextSearchProductData(user.Id, phone.Id, update)
		json.NewEncoder(w).Encode(searchProductData)
	} else if data_type == global.STORE_TYPE {
		var searchStoreData global.SearchStoreData
		searchStoreData, _ = database.PullNextSearchStoreData(user.Id, phone.Id, update)
		json.NewEncoder(w).Encode(searchStoreData)
	} else {
		badRequest(w, true, "datatype is not implemented!")
		return
	}
}

func PushData(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var user global.User
	var phone global.Phone
	var update int64
	var data_type int
	var sync_id int64

	global.MyPrint("#")
	global.MyPrint("PushData: " + params["datatype"])

	var userGuid, _ = uuid.FromString(params["userGuid"])
	var phoneGuid, _ = uuid.FromString(params["phoneGuid"])

	user, err = database.GetUser(userGuid)
	if err != nil {
		badRequest(w, true, "userGuid does not exist!")
		return
	}
	phone, err = database.GetPhone(userGuid, phoneGuid)
	if err != nil {
		badRequest(w, true, "phoneGuid does not exist!")
		return
	}
	update, err = strconv.ParseInt(params["update"], 10, 64)
	if err != nil {
		badRequest(w, true, "update does not exist, or is not a number!")
		return
	}
	data_type, err = global.DataType(params["datatype"])
	if err != nil {
		badRequest(w, true, "datatype does not exist!")
		return
	}

	if data_type == global.RECEIPT_TYPE {
		var receiptData global.ReceiptData
		_ = json.NewDecoder(r.Body).Decode(&receiptData)
		setSynchronisation(receiptData.Synchronisation, data_type, user.Id, phone.Id, update)
		sync_id = database.PushNextReceiptData(receiptData)
		receiptData = database.GetReceiptData(sync_id)
		json.NewEncoder(w).Encode(receiptData)
	} else if data_type == global.PRODUCT_TYPE {
		var searchProductData global.SearchProductData
		_ = json.NewDecoder(r.Body).Decode(&searchProductData)
		setSynchronisation(searchProductData.Synchronisation, data_type, user.Id, phone.Id, update)
		sync_id = database.PushNextSearchProductData(searchProductData)
		searchProductData = database.GetSearchProductData(sync_id)
		json.NewEncoder(w).Encode(searchProductData)
	} else if data_type == global.STORE_TYPE {
		var searchStoreData global.SearchStoreData
		_ = json.NewDecoder(r.Body).Decode(&searchStoreData)
		setSynchronisation(searchStoreData.Synchronisation, data_type, user.Id, phone.Id, update)
		sync_id = database.PushNextSearchStoreData(searchStoreData)
		searchStoreData = database.GetSearchStoreData(sync_id)
		json.NewEncoder(w).Encode(searchStoreData)
	} else {
		badRequest(w, true, "datatype is not implemented!")
		return
	}
}

func setSynchronisation(sync *global.Synchronisation, data_type int, user_id int64, phone_id int64, update int64) {
	sync.Data_Type = data_type
	sync.User_Id = user_id
	sync.Phone_Id = phone_id
	sync.Update = update
}

func badRequest(w http.ResponseWriter, isJson bool, err string) {
	if !isJson {
		http.Error(w, err, http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprintf(w, `{"result":"","error":%q}`, err)
}








func PushData2(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var phone global.Phone
	var update int64
	var data_type int
	var sync_id int64

	global.MyPrint("#")
	global.MyPrint("PushData: " + params["datatype"])

	phone, err = database.RegisterPhone2(params["username"], params["password"], params["phonename"])
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}
	update, err = strconv.ParseInt(params["update"], 10, 64)
	if err != nil {
		badRequest(w, true, "update does not exist, or is not a number!")
		return
	}
	data_type, err = global.DataType(params["datatype"])
	if err != nil {
		badRequest(w, true, "datatype does not exist!")
		return
	}

	if data_type == global.RECEIPT_TYPE {
		var receiptData global.ReceiptData
		_ = json.NewDecoder(r.Body).Decode(&receiptData)
		setSynchronisation(receiptData.Synchronisation, data_type, phone.User_Id, phone.Id, update)
		sync_id = database.PushNextReceiptData(receiptData)
		receiptData = database.GetReceiptData(sync_id)
		json.NewEncoder(w).Encode(receiptData)
	} else if data_type == global.PRODUCT_TYPE {
		var searchProductData global.SearchProductData
		_ = json.NewDecoder(r.Body).Decode(&searchProductData)
		setSynchronisation(searchProductData.Synchronisation, data_type, phone.User_Id, phone.Id, update)
		sync_id = database.PushNextSearchProductData(searchProductData)
		searchProductData = database.GetSearchProductData(sync_id)
		json.NewEncoder(w).Encode(searchProductData)
	} else if data_type == global.STORE_TYPE {
		var searchStoreData global.SearchStoreData
		_ = json.NewDecoder(r.Body).Decode(&searchStoreData)
		setSynchronisation(searchStoreData.Synchronisation, data_type, phone.User_Id, phone.Id, update)
		sync_id = database.PushNextSearchStoreData(searchStoreData)
		searchStoreData = database.GetSearchStoreData(sync_id)
		json.NewEncoder(w).Encode(searchStoreData)
	} else {
		badRequest(w, true, "datatype is not implemented!")
		return
	}
}


func PullData2(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var phone global.Phone
	var update int64
	var data_type int

	global.MyPrint("#")
	global.MyPrint("PullData: " + params["datatype"])

	phone, err = database.RegisterPhone2(params["username"], params["password"], params["phonename"])
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}
	update, err = strconv.ParseInt(params["update"], 10, 64)
	if err != nil {
		badRequest(w, true, "update does not exist, or is not a number!")
		return
	}
	data_type, err = global.DataType(params["datatype"])
	if err != nil {
		badRequest(w, true, "datatype does not exist!")
		return
	}

	if data_type == global.RECEIPT_TYPE {
		var receiptData global.ReceiptData
		receiptData, _ = database.PullNextReceiptData(phone.User_Id, phone.Id, update)
		json.NewEncoder(w).Encode(receiptData)
	} else if data_type == global.PRODUCT_TYPE {
		var searchProductData global.SearchProductData
		searchProductData, _ = database.PullNextSearchProductData(phone.User_Id, phone.Id, update)
		json.NewEncoder(w).Encode(searchProductData)
	} else if data_type == global.STORE_TYPE {
		var searchStoreData global.SearchStoreData
		searchStoreData, _ = database.PullNextSearchStoreData(phone.User_Id, phone.Id, update)
		json.NewEncoder(w).Encode(searchStoreData)
	} else {
		badRequest(w, true, "datatype is not implemented!")
		return
	}
}
