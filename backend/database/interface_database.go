package database

import (
	"errors"

	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"

	_ "github.com/lib/pq"

	"strconv"

	"budget/global"
	"budget/passwords"
)

// ### USER ######################################################################################

func RegisterUser(userName string, password string) (global.User, error) {
	var user global.User
	var err error

	user, err = selectUser_ByName(userName)

	if err != nil {
		var emptyUser global.User
		return emptyUser, errors.New("Error: User authentication failed, user does not exist!")
	} else {
		if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
			var emptyUser global.User
			return emptyUser, errors.New("Error: User authentication failed, password is not correct!")
		}
		user.Password = "CLASSIFIED"
		return user, nil
	}
}

func GetUser(userGuid uuid.UUID) (global.User, error) {
	return selectUser_ByGuid(userGuid)
}

func GetUserInfo(userName string) (global.User, error) {
	return selectUser_ByName(userName)
}

func UpdateUserType(userName string, userType int) (global.User, error) {
	user, err := selectUser_ByName(userName)
	if err != nil {
		return user, err
	}
	user.User_Type = userType
	updateUserType(user)
	if err != nil {
		return user, err
	}
	return user, nil

}

func CreateUserPassword(userName string, password string, userType int, update bool) (global.User, error) {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), 8)
	return CreateUserEncryptedPassword(userName, string(hashedPassword), userType, update)
}

func CreateUserEncryptedPassword(userName string, cryptPassword string, userType int, update bool) (global.User, error) {
	var user global.User
	var err error
	var id int64

	user, err = selectUser_ByName(userName)

	if err != nil {
		user.Guid = uuid.NewV1()
		user.Name = userName
		user.Password = cryptPassword
		user.User_Type = userType
		user.Update = 0
		id, err = createUser(user)
		user, err = selectUser_ById(id)
		return user, nil
	} else {
		if update {
			user.Password = cryptPassword
			err = updateUserPassword(user)
			user, err = selectUser_ById(user.Id)
			return user, nil
		} else {
			return user, errors.New("Error: User already exsists!")
		}
	}
}

func CreateSuperUsers() {
	var user global.User
	user, err = CreateUserEncryptedPassword("Jack", "$2a$08$SnbY2RMLY5LZOKJhA00HR.UOlx4fZV4lub1wfHZ5dB9kZwWi5HmVe", 1, true)
	if err != nil {
		global.MyPrint(err.Error())
	}
	global.MyPrintTrace("User: " + user.Name)
}

func CreateUsers() {
	global.MyPrintTrace("START  - Creating users in the database ...")

	var nn int = 0
	var xx int = 0
	ups, outputFileName, update, err := passwords.GenerateUsersAndPasswords()
	if err != nil {
		global.MyPrintTrace(err.Error())
	} else {
		for i, up := range ups {
			_, err := CreateUserEncryptedPassword(up.Name, up.Hash, 0, update)
			if err != nil {
				ups[i].Status = false
				xx = xx + 1
			} else {
				nn = nn + 1
			}
		}
		if nn > 0 {
			passwords.CreatePasswordFiles(outputFileName, ups)
		}
	}

	global.MyPrintTrace("FINISH - " + strconv.Itoa(nn) + " users are created (" + strconv.Itoa(xx) + " exist!).")
}

// ### PHONE ######################################################################################

func RegisterPhone(userGuid uuid.UUID, phoneName string) (global.Phone, error) {
	var user global.User
	var phone global.Phone
	var err error
	var id int64

	user, err = selectUser_ByGuid(userGuid)
	if err != nil {
		return phone, errors.New("Error: User does not exist!")
	}

	phone, err = selectPhone_ByName(phoneName, user.Id)

	if err != nil {
		phone.Guid = uuid.NewV1()
		phone.User_Id = user.Id
		phone.Name = phoneName
		phone.Update = 0
		id, err = createPhone(phone)
		phone, err = selectPhone_ById(id)
		return phone, nil
	} else {
		return phone, nil //errors.New("Warning: Phone already exists!")
	}
}

func RegisterPhone2(userName string, password string, phoneName string) (global.Phone, error) {
	var user global.User
	var phone global.Phone
	var phones []global.Phone
	var err error
	var id int64

	user, err = RegisterUser(userName, password)
	if err != nil {
		global.MyPrintTrace("user err : " + userName + " " + password)
		return phone, err
	}

	var found bool = false
	phones, err = selectPhones_ByUserId(user.Id)
	if err == nil {
		for _, p := range phones {
			err = bcrypt.CompareHashAndPassword([]byte(p.Name), []byte(phoneName))
			if err == nil {
				phone = p
				found = true
				break
			}
		}
	}

	if ! found {
		hashedPhoneName, _ := bcrypt.GenerateFromPassword([]byte(phoneName), 8)

		phone.Guid = uuid.NewV1()
		phone.User_Id = user.Id
		phone.Name = string(hashedPhoneName)
		phone.Update = 0
		id, err = createPhone(phone)
		phone, err = selectPhone_ById(id)
		global.MyPrintTrace("new phone")
		return phone, nil
	} else {
		global.MyPrintTrace("old phone")
		return phone, nil //errors.New("Warning: Phone already exists!")
	}
}

func GetPhone(userGuid uuid.UUID, phoneGuid uuid.UUID) (global.Phone, error) {
	var user global.User
	var phone global.Phone
	var err error

	user, err = selectUser_ByGuid(userGuid)
	if err != nil {
		return phone, errors.New("Error: User does not exist!")
	}

	phone, err = selectPhone_ByGuid(phoneGuid, user.Id)

	if err != nil {
		return phone, errors.New("Error: Phone does not exist!")
	} else {
		return phone, nil
	}
}

// ### RECEIPT_DATA ######################################################################################

func PushNextReceiptData(receiptData global.ReceiptData) int64 {
	var user global.User
	var phone global.Phone
	var sync_id int64
	var updated bool
	var addData bool

	var errS error
	var errT error
	var errP error
	var errI error

	user, _ = selectUser_ById(receiptData.Synchronisation.User_Id)
	phone, _ = selectPhone_ById(receiptData.Synchronisation.Phone_Id)

	var pushed int64 = receiptData.Synchronisation.Update
	var latest int64 = user.Update
	latest = latest + 1
	receiptData.Synchronisation.Update = latest

	//BEGIN TRANSACTION #####################################
	global.MyPrint("Begin Transaction")
	var validTransaction bool = true
	tx, err := con.Begin()
	if err != nil {
		validTransaction = false
	} else {

		addData = false

		var synchronisation global.Synchronisation
		synchronisation, errS = selectSynchronisation_ByDataIdentifier(tx,
			receiptData.Synchronisation.Data_Identifier, receiptData.Synchronisation.User_Id)

		if errS != nil {
			sync_id, errS = createSynchronisation(tx, receiptData.Synchronisation)
			if errS != nil {
				validTransaction = false
			} else {
				addData = true
			}
		} else {
			sync_id = synchronisation.Id
			receiptData.Synchronisation.Id = synchronisation.Id
			updated, err = updateSynchronisation(tx, receiptData.Synchronisation)
			if err != nil {
				validTransaction = false
			} else {
				if updated {
					errI = deleteImage(tx, sync_id)
					errP = deleteProducts(tx, sync_id)
					errT = deleteTransaction(tx, sync_id)
					if errT != nil || errP != nil || errI != nil {
						validTransaction = false
					} else {
						addData = true
					}
				}
			}
		}

		if validTransaction {
			if addData {
				errT = createTransaction(tx, sync_id, receiptData.Transaction)
				for _, product := range receiptData.Products {
					errP = createProduct(tx, sync_id, &product)
					if errP != nil {
						break
					}
				}
				if receiptData.Transaction.ReceiptLocation != "" {
					errI = createImage(tx, sync_id, receiptData.Image)
					if errT != nil || errP != nil || errI != nil {
						validTransaction = false
					}
				}
			}
		}

		if validTransaction {
			user.Update = latest
			err = updateUserUpdate(tx, user)
			if err != nil {
				validTransaction = false
			} else {
				phone.Update = pushed
				err = updatePhone(tx, phone)
				if err != nil {
					validTransaction = false
				}
			}
		}
	}

	if validTransaction {
		tx.Commit()
		global.MyPrint("& Commit Transaction")
	} else {
		tx.Rollback()
		global.MyPrint("! Rollback Transaction")
	}
	//END TRANSACTION #######################################

	return sync_id
}

func PullNextReceiptData(user_id int64, phone_id int64, pulled_update int64) (global.ReceiptData, error) {
	var synchronisation global.Synchronisation
	var transaction global.Transaction
	var products []global.Product
	var image global.Image

	synchronisation, err = selectSynchronisation_NextToPull(global.RECEIPT_TYPE, user_id, phone_id, pulled_update)

	if synchronisation.Action != global.DELETE {
		transaction, err = selectTransaction(synchronisation.Id)
		products, err = selectProducts(synchronisation.Id)
		if transaction.ReceiptLocation != "" {
			image, err = selectImage(synchronisation.Id)
		} else {
			image.TransactionID = transaction.TransactionID
			image.Base64image = ""
		}
	}

	var receiptData global.ReceiptData
	receiptData.Synchronisation = &synchronisation
	receiptData.Transaction = &transaction
	receiptData.Products = products
	receiptData.Image = &image

	return receiptData, nil
}

func GetReceiptData(sync_id int64) global.ReceiptData {
	var synchronisation global.Synchronisation
	var transaction global.Transaction
	var products []global.Product
	var image global.Image

	synchronisation, _ = selectSynchronisation_ById(sync_id)
	transaction, _ = selectTransaction(synchronisation.Id)
	products, _ = selectProducts(synchronisation.Id)
	if transaction.ReceiptLocation != "" {
		image, _ = selectImage(synchronisation.Id)
	} else {
		image.TransactionID = transaction.TransactionID
		image.Base64image = ""
	}

	var receiptData global.ReceiptData
	receiptData.Synchronisation = &synchronisation
	receiptData.Transaction = &transaction
	receiptData.Products = products
	receiptData.Image = &image

	return receiptData
}

// ### SEARCH_PRODUCT_DATA ######################################################################################

func PushNextSearchProductData(searchProductData global.SearchProductData) int64 {
	var user global.User
	var phone global.Phone
	var sync_id int64
	var updated bool
	var addData bool

	var errS error
	var errP error

	user, _ = selectUser_ById(searchProductData.Synchronisation.User_Id)
	phone, _ = selectPhone_ById(searchProductData.Synchronisation.Phone_Id)

	var pushed int64 = searchProductData.Synchronisation.Update
	var latest int64 = user.Update
	latest = latest + 1
	searchProductData.Synchronisation.Update = latest

	//BEGIN TRANSACTION #####################################
	global.MyPrint("Begin Transaction")
	var validTransaction bool = true
	tx, err := con.Begin()
	if err != nil {
		validTransaction = false
	} else {

		addData = false

		var synchronisation global.Synchronisation
		synchronisation, errS = selectSynchronisation_ByDataIdentifier(tx,
			searchProductData.Synchronisation.Data_Identifier, searchProductData.Synchronisation.User_Id)

		if errS != nil {
			sync_id, errS = createSynchronisation(tx, searchProductData.Synchronisation)
			if errS != nil {
				validTransaction = false
			} else {
				addData = true
			}
		} else {
			sync_id = synchronisation.Id
			searchProductData.Synchronisation.Id = synchronisation.Id
			updated, err = updateSynchronisation(tx, searchProductData.Synchronisation)
			if err != nil {
				validTransaction = false
			} else {
				if updated {
					errP = deleteSearchProduct(tx, sync_id)
					if errP != nil {
						validTransaction = false
					} else {
						addData = true
					}
				}
			}
		}

		if validTransaction {
			if addData {
				errP = createSearchProduct(tx, sync_id, searchProductData.SearchProduct)
			}
		}

		if validTransaction {
			user.Update = latest
			err = updateUserUpdate(tx, user)
			if err != nil {
				validTransaction = false
			} else {
				phone.Update = pushed
				err = updatePhone(tx, phone)
				if err != nil {
					validTransaction = false
				}
			}
		}
	}

	if validTransaction {
		tx.Commit()
		global.MyPrint("& Commit Transaction")
	} else {
		tx.Rollback()
		global.MyPrint("! Rollback Transaction")
	}
	//END TRANSACTION #######################################

	return sync_id
}

func PullNextSearchProductData(user_id int64, phone_id int64, pulled_update int64) (global.SearchProductData, error) {
	var synchronisation global.Synchronisation
	var searchProduct global.SearchProduct

	synchronisation, err = selectSynchronisation_NextToPull(global.PRODUCT_TYPE, user_id, phone_id, pulled_update)

	if synchronisation.Action != global.DELETE {
		searchProduct, err = selectSearchProduct(synchronisation.Id)
	}

	var searchProductData global.SearchProductData
	searchProductData.Synchronisation = &synchronisation
	searchProductData.SearchProduct = &searchProduct

	return searchProductData, nil
}

func GetSearchProductData(sync_id int64) global.SearchProductData {
	var synchronisation global.Synchronisation
	var searchProduct global.SearchProduct

	synchronisation, _ = selectSynchronisation_ById(sync_id)
	searchProduct, _ = selectSearchProduct(synchronisation.Id)

	var searchProductData global.SearchProductData
	searchProductData.Synchronisation = &synchronisation
	searchProductData.SearchProduct = &searchProduct

	return searchProductData
}

// ### SEARCH_STORE_DATA ######################################################################################

func PushNextSearchStoreData(searchStoreData global.SearchStoreData) int64 {
	var user global.User
	var phone global.Phone
	var sync_id int64
	var updated bool
	var addData bool

	var errS error
	var errP error

	user, _ = selectUser_ById(searchStoreData.Synchronisation.User_Id)
	phone, _ = selectPhone_ById(searchStoreData.Synchronisation.Phone_Id)

	var pushed int64 = searchStoreData.Synchronisation.Update
	var latest int64 = user.Update
	latest = latest + 1
	searchStoreData.Synchronisation.Update = latest

	//BEGIN TRANSACTION #####################################
	global.MyPrint("Begin Transaction")
	var validTransaction bool = true
	tx, err := con.Begin()
	if err != nil {
		validTransaction = false
	} else {

		addData = false

		var synchronisation global.Synchronisation
		synchronisation, errS = selectSynchronisation_ByDataIdentifier(tx,
			searchStoreData.Synchronisation.Data_Identifier, searchStoreData.Synchronisation.User_Id)

		if errS != nil {
			sync_id, errS = createSynchronisation(tx, searchStoreData.Synchronisation)
			if errS != nil {
				validTransaction = false
			} else {
				addData = true
			}
		} else {
			sync_id = synchronisation.Id
			searchStoreData.Synchronisation.Id = synchronisation.Id
			updated, err = updateSynchronisation(tx, searchStoreData.Synchronisation)
			if err != nil {
				validTransaction = false
			} else {
				if updated {
					errP = deleteSearchStore(tx, sync_id)
					if errP != nil {
						validTransaction = false
					} else {
						addData = true
					}
				}
			}
		}

		if validTransaction {
			if addData {
				errP = createSearchStore(tx, sync_id, searchStoreData.SearchStore)
				if errP != nil {
					validTransaction = false
				} 
			}
		}

		if validTransaction {
			user.Update = latest
			err = updateUserUpdate(tx, user)
			if err != nil {
				validTransaction = false
			} else {
				phone.Update = pushed
				err = updatePhone(tx, phone)
				if err != nil {
					validTransaction = false
				}
			}
		}
	}

	if validTransaction {
		tx.Commit()
		global.MyPrint("& Commit Transaction")
	} else {
		tx.Rollback()
		global.MyPrint("! Rollback Transaction")
	}
	//END TRANSACTION #######################################

	return sync_id
}

func PullNextSearchStoreData(user_id int64, phone_id int64, pulled_update int64) (global.SearchStoreData, error) {
	var synchronisation global.Synchronisation
	var searchStore global.SearchStore

	synchronisation, err = selectSynchronisation_NextToPull(global.STORE_TYPE, user_id, phone_id, pulled_update)

	if synchronisation.Action != global.DELETE {
		searchStore, err = selectSearchStore(synchronisation.Id)
	}

	var searchStoreData global.SearchStoreData
	searchStoreData.Synchronisation = &synchronisation
	searchStoreData.SearchStore = &searchStore

	return searchStoreData, nil
}

func GetSearchStoreData(sync_id int64) global.SearchStoreData {
	var synchronisation global.Synchronisation
	var searchStore global.SearchStore

	synchronisation, _ = selectSynchronisation_ById(sync_id)
	searchStore, _ = selectSearchStore(synchronisation.Id)

	var searchStoreData global.SearchStoreData
	searchStoreData.Synchronisation = &synchronisation
	searchStoreData.SearchStore = &searchStore

	return searchStoreData
}

// #########################################################################################
